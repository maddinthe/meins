package mai_15;

import java.util.*;

public class SportSoftware {
	private Map<String, Map<Simpson, Integer>> leistungen = new HashMap<>();
	private Set<Simpson> teilnehmer = new HashSet<>();

	public void neuerDatensatz(String s) {
		String[] splitt = s.split(",");
		Simpson simpson = new Simpson(splitt[0]);
		String disziplin = splitt[1];
		int wert = Integer.parseInt(splitt[2]);
		teilnehmer.add(simpson);
		Map<Simpson, Integer> werte = leistungen.get(disziplin);
		if (werte == null) {
			werte = new HashMap<>();
			werte.put(simpson, wert);
			leistungen.put(disziplin, werte);
		} else {
			Integer alt = werte.get(simpson);
			if (alt == null||new Vergleicher(disziplin).compare(wert, alt)>0)
				werte.put(simpson, wert);
		}
	}
	public int getTeilnehmerzahl(){
		return teilnehmer.size();
	}
	
	public List<Simpson> getTeilnehmer(){
		return new ArrayList<>(teilnehmer);
		
	}
	public List<String> getDisziplinen(){
		return new ArrayList<>(leistungen.keySet());
		
	}
	

	private class Vergleicher implements Comparator<Integer> {
		private boolean kleinerIstBesser;

		public Vergleicher(String disziplin) {
			kleinerIstBesser = disziplin.toLowerCase().contains("lauf");
		}

		public int compare(Integer i1, Integer i2) {
			return kleinerIstBesser ? i2 - i1 : i1 - i2;
		}
	}
}
