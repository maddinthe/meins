package februar_15;

public class Mama {
	private int id;
	private Kind k;
	
	
	public int getId() {
		return id;
	}
	public String toString(){
		return id+";"+k;
	}
public Mama(int i){
	id=i;
	k=new Kind(i*100+1);
}
	
public void tauschKind(Mama m){
	Kind temp=m.k;
	m.k=k;
	k=temp;
}
	
	private class Kind{
		int id;
		public Kind(int i){
			id=i;
		}
		public String toString(){
			return id+";"+Mama.this.id;
		}
		
	}
}
