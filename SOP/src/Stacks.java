import java.util.Stack;

public class Stacks {

	private Stack<Character> charStack = new Stack<Character>();
	private Stack charStack2 =new Stack();
	private Stack<Integer> intStack = new Stack<Integer>();
	private Stack<Object> objStack = new Stack<Object>();

	public static void main(String[] args) {

		Stacks s = new Stacks();
		s.charStack1(12);
		s.charStack2(12);
		s.intStack(12);
		s.objStack(12);
	}

	public void charStack1(int laenge) {
		for (int i = 0; i < laenge; i++)
			charStack.push((char) (((int) (Math.random() * 26) + 'a')));
		while (!charStack.isEmpty())
			System.out.print(charStack.pop()+", ");
		System.out.println();
	}
	public void charStack2(int laenge) {
		for (int i = 0; i < laenge; i++)
			charStack2.push((char) (((int) (Math.random() * 26) + 'a')));
		while (!charStack2.isEmpty())
			System.out.print(charStack2.pop()+", ");
		System.out.println();
	}
	

	public void intStack (int laenge){
		for (int i=0; i<laenge; i++)
			intStack.push((int)(Math.random()*10));
		while(!intStack.isEmpty())
			System.out.print(intStack.pop()+", ");
		System.out.println();
	}
	public void objStack(int laenge){
		for (int i=0;i<laenge;i++)
			objStack.push(new Object());
		while(!objStack.isEmpty())
			objStack.pop();
	}
}
