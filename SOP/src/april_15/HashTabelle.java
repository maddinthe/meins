package april_15;

public class HashTabelle<T> {
	Liste<T>[] eimer;

	@SuppressWarnings("unchecked")
	public HashTabelle(int groesse) {
		eimer = new Liste[groesse];
	}
	
	public void add (T value){
		int nr=(value.hashCode()&0x7fffffff)%eimer.length;
		if (eimer[nr]==null)
			eimer[nr]=new Liste<T>();
		if (!eimer[nr].contains(value))
			eimer[nr].add(value);		
	}
	
	public boolean contains(T value){	
		int nr=(value.hashCode()&0x7fffffff)%eimer.length;
		if (eimer[nr]==null)
			return false;
		if (eimer[nr].contains(value))
			return true;
		return false;
	}
}
