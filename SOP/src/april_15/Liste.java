package april_15;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

public class Liste<T> implements Collection<T> {

	private Element<T> first;
	private int size=0;

	public String toString() {
		String ret = "[";
		for (T temp : this)
			ret += temp + ", ";
		ret = (ret.length() == 1) ? (ret += "]") : (ret = ret.substring(0,
				ret.length() - 2)
				+ "]");
		return ret;
	}

	public T pop() {
		T ret = first.value;
		first = first.next;
		size--;
		return ret;
	}

	public boolean add(T value) {
		if (isEmpty())
			first = new Element<T>(value);
		else {
			Element<T> temp = new Element<T>(value);
			temp.next = first;
			first = temp;
		}
		size++;
		return true;
	}

	public boolean addAll(Collection<? extends T> c) {
		for (T temp : c) {
			add(temp);
		}
		return true;
	}

	public void clear() {
		first = null;
		size=0;

	}

	public boolean contains(Object o) {
		for (T temp : this)
			if (o.equals(temp))
				return true;
		return false;
	}

	public boolean containsAll(Collection<?> c) {
		for (Object obj : c)
			if (!this.contains(obj))
				return false;
		return true;
	}

	public boolean isEmpty() {
		return first == null;
	}

	public Iterator<T> iterator() {
		return new Aufzaehler();
	}

	public boolean remove(Object o) {
		if (first.value.equals(o)) {
			first = first.next;
			size--;
			return true;
		}
		for (Element<T> temp = first; temp.next != null; temp = temp.next)
			if (temp.next.value.equals(o)) {
				temp.next = temp.next.next;
				size--;
				return true;
			}
		return false;
	}

	public boolean removeAll(Collection<?> c) {
		boolean ret = false;
		for (Object o : c)
			while (this.contains(o)) {
				remove(o);
				ret = true;
			}
		return ret;
	}

	public boolean retainAll(Collection<?> c) {
		Liste<T> l = new Liste<>();
		for (T temp : this)
			if (c.contains(temp))
				l.addEnd(temp);
		boolean ret = l.size() == this.size();
		first = l.first;
		size=l.size;
		return !ret;
	}

	public int size() {
		return size;
	}

	public Object[] toArray() {
		Object[] ret = new Object[size()];
		int i = 0;
		for (T temp : this)
			ret[i++] = temp;
		return ret;
	}
	public void addEnd(T value){
		if (first==null) first=new Element<>(value);
		else{
		Element<T> temp = first;
		for (; temp.next != null; temp = temp.next);
		temp.next=new Element<>(value);}
		size++;
	}


	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> T[] toArray(T[] a) {		
		if (size>a.length)
			return (T[])Arrays.copyOf(toArray(),size,a.getClass());
		System.arraycopy(toArray(), 0, a, 0, size);
		if (a.length>size)
			a[size]=null;
		return a;
		
	}
	
	public void sort(){
		first=sort(this).first;
	}
	@SuppressWarnings("unchecked")
	private Liste<T> sort(Liste<T> list){
		if(list.first==null||list.first.next==null)return list;
		Liste<T> links=new Liste<>();
		Liste<T> rechts=new Liste<>();
		T pivot=list.pop();
		for (T temp:list)
			if (((Comparable)pivot).compareTo(temp)<0)
				rechts.add(temp);
			else links.add(temp);
		rechts.sort();
		links.sort();
		
		links.addEnd(pivot);
		for (T temp:rechts)
			links.addEnd(temp);
		return links;
	}
	
	public void sort(Comparator<T> comp){
		first=sort(comp, this).first;
	}

	private Liste<T> sort(Comparator<T> comp, Liste<T> list) {
		if(list.first==null||list.first.next==null)return list;
		Liste<T> links=new Liste<>();
		Liste<T> rechts=new Liste<>();
		T pivot=list.pop();
		for (T temp:list)
			if (comp.compare(pivot, temp)<0)
				rechts.add(temp);
			else links.add(temp);
		rechts.sort(comp);
		links.sort(comp);
		
		links.addEnd(pivot);
		for (T temp:rechts)
			links.addEnd(temp);
		return links;
	}




	private class Aufzaehler implements Iterator<T> {
		private Element<T> current = first;

		public boolean hasNext() {
			return current != null;
		}

		@Override
		public void remove() {
		}

		public T next() {
			T ret = (T) current.value;
			current = current.next;
			return ret;

		}

	}

	private static class Element<T> {
		private T value;
		private Element<T> next;

		public Element(T value) {
			this.value = value;
		}

	}

}
