package april_15;

public class IfModTest {

	public static void main(String[] args) {

		boolean bol;
		int in = 0;
		
		long start=System.currentTimeMillis();
		long h = 999999999l;
		while (h > 0) {
//			in++;
			if (++in == 100) in=0;
			h--;
		}
		
		System.out.println(System.currentTimeMillis()-start);
		h=999999999l;
		in = 0;
		start=System.currentTimeMillis();
		while (h > 0) {
			//in++;
			in = ++in%100;
			h--;
		}
		System.out.println(System.currentTimeMillis()-start);
	}

}
