package april_15;

import java.util.Arrays;

public class HashTabelle2<T> {
	private T[] eimer;

	@SuppressWarnings("unchecked")
	public HashTabelle2(int laenge) {
		this.eimer = (T[]) new Object[laenge];
	}

	
	public String toString() {
		return "HashTabelle2 [eimer=" + Arrays.toString(eimer) + "]";
	}


	public void add(T value) {
		int index = (value.hashCode() & 0x7fffffff) % eimer.length;
		int abbruch = index;
		while (eimer[index] != null) {
			if (eimer[index].equals(value))
				return;
			index = ++index % eimer.length;
			if (abbruch == index)
				eimer[-1] = null;
		}
		eimer[index] = value;
	}

	public boolean contains(T value) {
		int index = (value.hashCode() & 0x7fffffff) % eimer.length;
		int abbruch = index;
		while (eimer[index] != null) {
			if (eimer[index].equals(value))
				return true;
			index = ++index % eimer.length;
			if (abbruch == index)
				return false;
		}
		return false;
	}

	public void remove(T value) {
		int index = (value.hashCode() & 0x7fffffff) % eimer.length;
		int pos = index;
		while (!value.equals(eimer[pos])) {
			if (++pos==eimer.length)pos=0;
			if (eimer[pos] == null)
				return;
		}
		
		eimer[pos]=null;
			while(true){
				if(++pos==eimer.length)pos=0;
				if(eimer[pos]==null)break;
				T temp=eimer[pos];
				eimer[pos]=null;
				add(temp);		
			}
		
		
		
		
//		int del = pos;
//		while (eimer[pos]!=null&&(eimer[pos].hashCode() & 0x7fffffff) % eimer.length == index)
//			if (++pos==eimer.length)pos=0;
//		eimer[del] = null;
//		if (pos == del)
//			return;
//		eimer[del] = eimer[pos];
//		eimer[pos] = null;
	}

}
