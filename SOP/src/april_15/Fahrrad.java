package april_15;

public class Fahrrad implements Comparable<Fahrrad> {
	private String typ;
	private int schaltung;
	private String farbe;
	private int groesse;
	private int km;

	public Fahrrad(String typ, int schaltung, String farbe, int groesse, int km) {
		super();
		this.typ = typ;
		this.schaltung = schaltung;
		this.farbe = farbe;
		this.groesse = groesse;
		this.km = km;
	}

	public int getKm() {
		return km;
	}

	public void setKm(int km) {
		this.km = km;
	}

	public String getTyp() {
		return typ;
	}

	public int getSchaltung() {
		return schaltung;
	}

	public String getFarbe() {
		return farbe;
	}

	public int getGroesse() {
		return groesse;
	}

	public int compareTo(Fahrrad arg0) {
		int r=this.typ.compareTo(arg0.typ);
		if (r!=0)return r;
		r=this.schaltung-arg0.schaltung;
		if (r!=0)return r;
		r=this.farbe.compareTo(farbe);
		if (r!=0)return r;
		return this.groesse-arg0.groesse;
	}
	
	public boolean equals(Object o){
		if (o==null) return false;
		if (!(o instanceof Fahrrad)) return false;
		Fahrrad f= (Fahrrad)o;
		return (typ.equals(f.typ)&&(schaltung==f.schaltung)&&farbe.equals(f.farbe)&&(groesse==f.groesse));		
	}
	public int hashCode(){	
		return typ.hashCode()^schaltung^farbe.hashCode()^groesse;
	}


}
