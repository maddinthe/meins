package maerz_15;

import java.util.Comparator;


public class Auto implements Comparable<Auto>{
	public static final Comparator<Auto> HUBRAUM_SORT=new Comparator<Auto>(){
		public int compare(Auto a1, Auto a2) {
			if (a1.hubraum==a2.hubraum)return a1.kennzeichen.compareTo(a2.kennzeichen);
			return a1.hubraum-a2.hubraum;
		}};
	private String kennzeichen;
	private int hubraum;
	
	public Auto(String kennzeichen, int hubraum){
		this.kennzeichen=kennzeichen;
		this.hubraum=hubraum;
		
	}

	public String getKennzeichen() {
		return kennzeichen;
	}

	public int getHubraum() {
		return hubraum;
	}

	
	public int compareTo(Auto o) {	
		return o.kennzeichen.compareTo(kennzeichen);
	}

	
	public String toString(){
		return "Kennzeichen: "+kennzeichen+", Hubraum: "+hubraum;
	}
	
	
	
}
