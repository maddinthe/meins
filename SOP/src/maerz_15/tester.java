package maerz_15;

//import java.util.Comparator;

public class tester {
	public static void main(String[] args) {
		Liste<Auto> l = new Liste<>();

		for (int i = 0; i < 10; i++) {
			l.push(new Auto(baueKennzeichen(),
					((int) (Math.random() * 20 + 10) * 100)));
		}
		for (Auto a : l)
			System.out.println(a);
//		class Hubraumvergleicher implements Comparator<Auto> {
//			public int compare(Auto a1, Auto a2) {
//				if (a1.getHubraum() - a2.getHubraum() == 0)
//					return a1.compareTo(a2);
//				return a1.getHubraum() - a2.getHubraum();
//			}
//
//		}

//		l.sort(new Comparator<Auto>(){
//			public int compare(Auto a1, Auto a2){
//				return a1.getHubraum()-a2.getHubraum();
//			}
//		});
		l.sort(Auto.HUBRAUM_SORT);
		System.out.println();
		for (Auto a : l)
			System.out.println(a);

	}

	private static String baueKennzeichen() {
		String ret = "" + ((char) ((Math.random() * 26) + 'A'));
		if (Math.random() > 0.5)
			ret += ((char) ((Math.random() * 26) + 'A'));

		ret += " " + ((char) ((Math.random() * 26) + 'A'));
		if (Math.random() > 0.5)
			ret += ((char) ((Math.random() * 26) + 'A'));

		return ret + " " + (int) (Math.random() * 1000);
	}
}
