package maerz_15;

import java.text.Collator;
import java.util.Comparator;
//import java.util.Comparator;
//import java.util.Locale;

public class Zeitungstester {

	public static void main(String[] args) {
		String text =
				"Russland sei an Krieg nicht interessiert "+
				"Russlands Pr�sident Wladimir Putin sieht nach den "+
				"Minsker Friedensgespr�chen f�r die Ostukraine eine "+
				"Chance f�r eine schrittweise Normalisierung der Lage "+
				"im Kriegsgebiet Donbass."+
				" "+
				"\"Wenn das Minsker Abkommen f�r eine L�sung des "+
				"Konflikts in der Ostukraine erf�llt wird, dann bin "+
				"ich �berzeugt, dass die Situation sich schrittweise "+
				"normalisiert\", sagte Putin. Russland sei wie Europa "+
				"nicht an Krieg interessiert, betonte der Pr�sident "+
				"nach Kreml-Angaben in einem Interview des "+
				"Staatsfernsehens."+
				" "+
				"\"Ich denke, dass ein solch apokalyptisches Szenario "+
				"wohl kaum m�glich ist - und hoffe, dass es dazu nicht "+
				"kommt\", sagte der Kremlchef. Demnach sehe Putin auch "+
				"Anzeichen daf�r, dass durch die Verhandlungen in "+
				"Minsk allm�hlich wieder Vertrauen zwischen Russland, "+
				"Deutschland und Frankreich entstehe. Er erkl�rte "+
				"zudem, ein weiteres Treffen mit Deutschland, "+
				"Frankreich und der Ukraine zum Ukraine-Konflikt sei "+
				"nicht erforderlich. Er hoffe, dass die zuletzt in "+
				"Minsk getroffenen Verabredungen umgesetzt w�rden.";
		
		String[] temp=text.split(" ");
		Liste<String> l=new Liste<>();
		
		for(String s:temp)
			l.push(s);

		l.sort(String.CASE_INSENSITIVE_ORDER);
//		l.sort(new Comparator<String>(){
//			public int compare(String s1, String s2) {
//				Collator collator = Collator.getInstance(Locale.GERMAN);
//				return collator.compare(s1, s2);
//			}
//			
//		});
	//	l.sort((Comparator)Collator.getInstance());
		
		l.sort(new Comparator<String>(){
			public int compare(String o1, String o2) {
				return new StringBuilder(o1).reverse().toString().compareToIgnoreCase(new StringBuilder(o2).reverse().toString());
			}
		});
		
		for (String s:l)
			System.out.println(s);
		
		
	}

}
