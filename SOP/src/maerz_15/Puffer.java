package maerz_15;

import java.util.Iterator;

public class Puffer<T> implements Iterable<T> {

	
	private Element<T> schreiben;
	private Element<T> lesen;

	public Puffer(int groesse) {
		schreiben = new Element<T>();
		lesen = schreiben;
		for (; groesse > 0; groesse--) {
			schreiben.next = new Element<T>();
			schreiben = schreiben.next;
		}
		schreiben.next = lesen;
		schreiben = lesen;

	}

	public boolean isFull() {
		return schreiben.next == lesen;
	}

	public boolean isEmpty() {
		return schreiben == lesen;
	}

	public void schreiben(T value) {
		schreiben.value = value;
		schreiben = schreiben.next;
	}

	public T lesen() {
		T ret = lesen.value;
		lesen = lesen.next;
		return ret;
	}
	public Iterator<T> iterator() {
		return new Iterator<T>(){
			Element<T> current=lesen;
			Element<T> last=schreiben;
			public boolean hasNext() {
				return current!=last;
			}
			public T next() {
				T value=current.value;
				current=current.next;
				return value;
			}
			public void remove() {}
		};
	}

	private static class Element<S> {
		private Element<S> next;
		private S value;
	}
}
