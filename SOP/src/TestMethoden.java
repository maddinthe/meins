import java.util.Scanner;

public class TestMethoden {
	private HilfsMethoden hm = new HilfsMethoden();
	private Scanner s = new Scanner(System.in);

	public static void main(String[] args) {
		boolean nabbruch=true;
		while(nabbruch){
		Scanner s = new Scanner(System.in);
		TestMethoden t= new TestMethoden();
		System.out.println("Bitte w�hlen sie:");
		System.out.println("1: Strings vergleichen");
		System.out.println("2: Summen Bilden");
		System.out.println("3: Schniit Bilden");
		System.out.println("4: PQ formel");
		System.out.println("5: Quersumme");
		System.out.println("6: Zufallszahl");
		System.out.println("7: Zufallszahlenarray");
		System.out.println("8: Zufallstahlenarray als String ausgeben");
		System.out.println("9: Zwei Zufallsarays verbinden");
		System.out.println("Alles andere Bricht ab");
		int abfrage=s.nextInt();
		switch (abfrage){
		case 1:
			t.vergleichen();
			break;
		case 2:
			t.summe();
			break;
		case 3:
			t.schnitt();
			break;
		case 4:
			t.pq();
			break;
		case 5:
			t.quersumme();
			break;
		case 6:
			t.random();
			break;
		case 7:
			t.randomArray();
			break;
		case 8:
			t.randomArrayString();
			break;
		case 9:
			t.arrVerb();
			break;
		default:
			nabbruch=false;
			break;
		}}
	}

	
	public void vergleichen(){
		System.out.println(hm.vergleiche("Hallo","Hallo"));
	}
	public void summe(){
		int[] array={1,2,3,4,5,6,7,8};
		System.out.println(hm.summe(array));
	}
	public void schnitt(){
		int[] array={1,2,3,4,5,6,7,8};
		System.out.println(hm.schnitt(array));
	}
	public void pq() {
		System.out.println("L�sung f�r x�+px-q=0");
		System.out.println("Bitte p eingeben: ");
		double p = s.nextDouble();
		System.out.println("Biite q eingeben: ");
		double q = s.nextDouble();
		double[] array = hm.nullstellen(p, q);
		if (array == null)
			System.out.println("Es gibt keine Nullstelle");
		else if (array.length == 1)
			System.out.println("Es gibt genau eine Nullstelle bei X="
					+ array[0]);
		else {
			System.out.println("Es gibt zwei Nullstellen bei:");
			System.out.println("X1=" + array[0]);
			System.out.println("X2=" + array[1]);
		}
		
	}
public void quersumme(){
	System.out.println("Bitte Quersumme eingeben:");
		System.out.println(hm.quersumme(s.nextInt()));
}
	public void random(){
		System.out.println("Wie lang soll die Zufallszahl sein?");
		System.out.println(hm.random(s.nextInt()));
	}
	public void randomArray(){
		System.out.println("Wie Lang soll das Array sein?");
		int laenge =s.nextInt();
		int[] erg = hm.baue(laenge);
			for (int i = 0;i<laenge;i++)
				System.out.println("Zahl"+ (i+1) + " =" + erg[i]);
			
	}
	public void randomArrayString(){
		System.out.println("Wie lang soll das Array sein?");
		System.out.println(hm.toString(hm.baue(s.nextInt())));
	}
	public void arrVerb(){
		System.out.println("Wie lang ist das erste Array?");
		int[] ar1=hm.baue(s.nextInt());
		System.out.println("Wie lang ist das zweite Array?");
		int[] ar2=hm.baue(s.nextInt());
		System.out.println(hm.toString(hm.verbinde(ar1, ar2)));
	}
}
