public class Autohaus {
	private Auto[] autos;

	public void fuellen(int anzahl) {
		autos = new Auto[anzahl];
		String[] farben = { "rot", "wei�", "gr�n", "gelb", "blau" };
		for (int i = 0; i < anzahl; i++) {
			autos[i] = new Auto();
			autos[i].setFarbe(farben[(int) (Math.random() * 5)]);

		}
	}

	public int zaehle(String farbe) {
		int anzahl = 0;
		for (Auto a : autos)
			if ((a != null) && (a.getFarbe() != null)
					&& (a.getFarbe().equals(farbe)))
				anzahl++;
		return anzahl;
	}

	public void entferne(String farbe) {
		int anz=autos.length;
		for (int i = 0; i < autos.length; i++)
			if (farbe.equals(autos[i].getFarbe())){
				autos[i] = null;
				anz--;}
		Auto[] tempAuto = new Auto[anz];
		for (int i = 0,j=0; i < autos.length; i++)
			if (autos[i] != null) 
				tempAuto[j++] = autos[i];	
		autos = tempAuto;
	}

	public Auto[] zeige() {
		return autos;
	}
}
