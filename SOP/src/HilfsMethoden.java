public class HilfsMethoden {
	public boolean vergleiche(String s1, String s2) {
		if (s1.length() != s1.length())
			return false;
		for (int i = 0; i < s1.length(); i++)
			if (s1.charAt(i) != s2.charAt(i))
				return false;
		return true;
	}

	public int summe(int[] array) {
		int sum = 0;
		for (int z : array)
			sum += z;
		return sum;
	}

	public double schnitt(int[] array) {
		return (double) summe(array) / array.length;
	}

	public double[] nullstellen(double pP, double pQ) {
		double diskriminante = (pP / 2) * (pP / 2) - pQ;
		if (diskriminante < 0) {
			return null;
		}
		if (diskriminante == 0) {
			return new double[] { -pP / 2 };
		}
		double wurzel = Math.sqrt(diskriminante);
		return new double[] { -pP / 2 + wurzel, -pP / 2 - wurzel };
	}

	public int quersumme(int zahl) {
		int quersumme = 0;
		String qZahl = String.valueOf(zahl);
		for (int i = 0; i < qZahl.length(); i++)
			quersumme += Character.getNumericValue(qZahl.charAt(i));
		return quersumme;
	}

	public int[] baue(int laenge) {
		int[] zufall= new int[laenge];
		for (int i = 0; i < laenge; i++) 
			zufall[i]=(int) (Math.random() * 10);
		
		return zufall;
	}

	public int random(int laenge) {
		int multi = 10;
		for (int i = 1; i < laenge; i++) {
			multi = multi * 10;
		}
		int zufall = (int) (Math.random() * multi);
		return zufall;
	}
	public String toString (int[] arr){
		String erg="[";
			for(int i=0;i<arr.length;i++){
				erg+=arr[i];
				if(i<arr.length-1)
					erg+=',';
				}
		return erg+']';
	}
	public int[] verbinde(int[] ar1, int[]ar2){
		int[] erg= new int[ar1.length+ar2.length];
		int i=0;
		for (int z:ar1)
			erg[i++]=z;	
		for (int z:ar2)
			erg[i++]=z;
		return erg;
	}
}
