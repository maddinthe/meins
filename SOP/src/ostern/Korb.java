package ostern;

public class Korb {
	private Liste<Ei> korb=new Liste<>();
	
	public void eiRein(Ei ei){
		korb.push(ei);
	}
	
	public Ei eiRaus(){
		return korb.pop();
	}
	
	public int gewicht(){
		int gewicht=0;
		for (Ei ei:korb)
			gewicht+=ei.getGewicht();
		return gewicht;
	}
	

}
