package ostern;
public class Ei {
	private String farbe;
	private int gewicht;
	
	public Ei (int gewicht, String farbe){
		this.gewicht=gewicht;
		this.farbe=farbe;
	}

	public String getFarbe() {
		return farbe;
	}

	public int getGewicht() {
		return gewicht;
	}
	

}
