package ostern;

import java.util.Comparator;
import java.util.Iterator;

public class Liste<T> implements Iterable<T> {

	private Element<T> first;

	public boolean isEmpty() {
		return first == null;
	}

	public T pop() {
		T ret = first.value;
		first = first.next;

		return ret;
	}

	public void push(T value) {
		Element<T> e = new Element<T>(value);
		e.next = first;
		first = e;
	}

	public void addEnd(T value) {
		if (first==null)
			first=new Element<T>(value);
		else{
		Element<T> e = first;
		for (; e.next != null; e = e.next);
		e.next = new Element<T>(value);
		}
	}
	
	public boolean add(T value) {
		if (isEmpty())
			first = new Element<T>(value);
		else {
			Element<T> temp = new Element<T>(value);
			temp.next = first;
			first = temp;
		}
		return true;
	}
	
	
	public boolean addAll(Iterable<? extends T> c) {
		for (T temp : c) {
			add(temp);
		}
		return true;
	}
	
	
	
	

	public void delete(T value) {
		for (Element<T> e = first; e.next != null; e = e.next) {
			if (e.next.value == value) {
				e.next = e.next.next;
				return;
			}
		}
	}
	
	public void sort(Comparator<T> vergleicher) {
		if (first != null)
			first = sort(this,vergleicher).first;
	}

	private Liste<T> sort(Liste<T> list,Comparator<T> vergleicher) {
		if (list.first==null||list.first.next==null)return list;
		T pivot = list.pop();
		Liste<T> li = new Liste<T>();
		Liste<T> re = new Liste<T>();

		for (T temp : list) {
			if (vergleicher.compare(pivot,temp) > 0)
				li.push(temp);
			else
				re.push(temp);
		}
		li = sort(li,vergleicher);
		re = sort(re,vergleicher);
		li.addEnd(pivot);
		for (T temp : re)
			li.addEnd(temp);

		return li;
	}


	public void sort() {
		if (first != null)
			first = sort(this).first;
	}

	@SuppressWarnings("unchecked")
	private Liste<T> sort(Liste<T> list) {
		if (list.first==null||list.first.next==null)return list;
		T pivot = list.pop();
		Liste<T> li = new Liste<T>();
		Liste<T> re = new Liste<T>();

		for (T temp : list) {
			if (((Comparable<T>)temp).compareTo(pivot) < 0)
				li.push(temp);
			else
				re.push(temp);
		}
		li = sort(li);
		re = sort(re);
		li.addEnd(pivot);
		for (T temp : re)
			li.addEnd(temp);

		return li;
	}

	public String toString() {
		String ret="";
		for(T temp:this)
			ret=temp+", "+ret;
		if (ret.contains(", ")) return ret.substring(0,ret.length()-2);
		return ret;
		
	}

	public Iterator<T> iterator() {
		return new Aufzaehler();
	}

	private class Aufzaehler implements Iterator<T> {
		private Element<T> next = first;

		public boolean hasNext() {
			return next != null;
		}

		public T next() {
			T ret = next.value;
			next = next.next;
			return ret;
		}

		public void remove() {
		}

	}

	private static class Element<S> {
		private S value;
		private Element<S> next;

		public Element(S value) {
			this.value = value;
		}
	}

}
