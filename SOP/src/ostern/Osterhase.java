package ostern;

import java.util.Comparator;

public class Osterhase implements Comparable<Osterhase> {
	private Korb korb = new Korb();
	private int groesse;
	public static final Comparator<Osterhase> KORB_ORDER=new Comparator<Osterhase>(){
		public int compare(Osterhase o1, Osterhase o2) {
			int ret=o1.korb.gewicht()-o2.korb.gewicht();
				if (ret==0)return o1.compareTo(o2);
			return ret;
		}};

	public Osterhase(int groesse) {
		String[] farben = { "Lila", "Gelb", "Rot", "Blau", "Gr�n", "Rosa",
				"T�rkis" };
		for (int i = (int) (Math.random() * 9) + 7; i > 0; i--) {
			korb.eiRein(new Ei((int) (Math.random() * 31) + 50,
					farben[(int) (Math.random() * 7)]));
		}
		this.groesse = groesse;
	}
	public Osterhase(){
		this((int)(Math.random()*51)+50);
	}
	
	public int compareTo(Osterhase o) {
		return this.groesse-o.groesse;
	}
	public String toString() {
		return "Osterhase [Korbgewicht=" + korb.gewicht() + ", Groesse=" + groesse + "]";
	}
	
	
}
