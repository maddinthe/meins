package januar_15;

public class QSort {

	public static void main(String[] args) {
		// int[] bla = new int[50];
		// for (int i = 0; i < bla.length; i++)
		// bla[i] = (int) (Math.random() * 100);
		//
		// qSort(bla, 0, bla.length - 1);
		// for(int i:bla)
		// System.out.println(i);
//
//		String[][] test = { { "HS6", "Theilen" }, { "HS6", "Kertz" },
//				{ "HS1", "Marcks" }, { "HS1", "Kamps" }, { "HS2", "Bla" },
//				{ "HS3", "Heinz" },{"2","d"},{"2","b"},{"2","a"} };
//		qSortLexi(test, 0, test.length - 1);
//		ausg(test);
//		
//		
//		int[][] bla={{1,2,3},{4,5,6}};
//		ausg(bla);
//		System.out.println();
//		bla=dreh(bla);
//		ausg(bla);
//		System.out.println();
//		bla=dreh(bla);
//		ausg(bla);
//		System.out.println();
//		bla=dreh(bla);
//		ausg(bla);
//		System.out.println();
//		bla=dreh(bla);
//		ausg(bla);
		
		String drehen="hallo";
		String keinPlanin="Welt";
		String palin="Kayak";
		
		System.out.println(dreh(drehen));
		System.out.println(palin(palin));
		System.out.println(palin(keinPlanin));
		
		
	}
	
	public static String dreh(String wert){
		if(wert.length()<=1) return wert;
		return dreh(wert.substring(1))+wert.charAt(0);
	}

	public static boolean palin (String palindrom){
		return palindrom.equalsIgnoreCase(dreh(palindrom));
		
	}
	

	private static void ausg(String[][] bla) {
		for(int i=0;i<bla.length;i++){
			for (int j=0;j<bla[0].length;j++){
				System.out.print(bla[i][j]);
			}System.out.println();
		}
		
	}
	
	
	private static void ausg(int[][] bla) {
		for(int i=0;i<bla.length;i++){
			for (int j=0;j<bla[0].length;j++){
				System.out.print(bla[i][j]);
			}System.out.println();
		}
		
	}





	public static int[][] dreh(int[][] bla) {
		int [][] neu=new int[bla[0].length][bla.length];
		
		for(int i=0;i<bla[0].length;i++)
			for (int j=0;j<bla.length;j++)
				neu[bla[0].length-1-i][j]=bla[j][i];
		return neu;
		
	}



	public static void qSort(int[] bla, int l, int r) {
		int i = l, j = r;
		int pivot = bla[(l + r) / 2];

		while (i <= j) {
			while (bla[i] < pivot)
				i++;
			while (bla[j] > pivot)
				j--;

			if (i <= j) {
				int temp = bla[i];
				bla[i] = bla[j];
				bla[j] = temp;
				i++;
				j--;
			}
		}
		if (j > l)
			qSort(bla, l, j);
		if (i < r)
			qSort(bla, i, r);

	}

	public static void qSortLexi(String[][] bla, int l, int r) {
		int i=l,j=r;
		String[] pivot=bla[(l+r)/2];
		
		while (i<=j){
			while (bla[i][0].compareTo(pivot[0])<0||(bla[i][0].equals(pivot[0])&&bla[i][1].compareTo(pivot[1])<0)) i++;
			while (bla[j][0].compareTo(pivot[0])>0||(bla[j][0].equals(pivot[0])&&bla[j][1].compareTo(pivot[1])>0)) j--;
			
			if (i<=j){
				String[] temp=bla[i];
				bla[i]=bla[j];
				bla[j]=temp;
				i++;
				j--;
			}
		}
		if (j>l)qSortLexi(bla,l,j);
		if (i<r)qSortLexi(bla,i,r);
		
	}

}
