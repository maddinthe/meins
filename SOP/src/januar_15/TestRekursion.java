package januar_15;

public class TestRekursion {

	public static void main(String[] args) {

		int[] arrSchw = { 3, 10, 2, 3, 4, 5, 6, 7, 8, 9, -10, 8, 11, 12, 13,
				14, 15, 16, 17, 18, 19, -5 };
		System.out.println(min(arrSchw));

	}
	
	public static int min(int[] array){
		return min (array,1,array[0]);
	}
	private static int min(int[] array, int pos, int min) {
		if (pos >= array.length)
			return min;
		if (array[pos] < min)
			min = array[pos];
		return min(array, pos + 1, min);
	}

	public static int fak(int a) {
		if (a == 1)
			return 1;
		return mul(a, fak(a - 1));

		// summe aus 1+1*2+1*2*3+1*2*3*4
	}

	public static int mul(int a, int b) {
		if (b == 0)
			return 0;
		return sum(a, mul(a, b - 1));
	}

	public static int pot(int a, int b) {
		if (b == 0)
			return 1;
		return mul(a, pot(a, b - 1));
	}

	public static int sum(int a, int b) {
		if (b == 0)
			return a;
		return sum(a + 1, b - 1);
	}

	public static String tausch(String str) {
		if (str.length() > 1)
			return tausch(str.substring(1)) + str.charAt(0);
		else
			return str;
	}

	public static int fib(int x) {
		if (x <= 2)
			return 1;
		else
			return fib(x - 1) + fib(x - 2);

	}

	public static int fibSchl(int x) {
		int z = 1;
		for (int i = 2, vv = 1, v = 1; i < x; i++) {
			z = v + vv;
			vv = v;
			v = z;
		}
		return z;
	}

	public static void runterzaehlen(int zahl) {
		System.out.println(zahl);
		if (zahl > 0)
			runterzaehlen(zahl - 1);
		System.out.println(zahl);
	}

	public static int[] arrDreh(int[] arr){
		return arrDreh(arr,0);
	}
	private static int[] arrDreh(int[] arr, int pos) {
		if (pos >= arr.length)
			return new int[arr.length];
		int[] arrBlau = arrDreh(arr, pos + 1);
		arrBlau[arrBlau.length - 1 - pos] = arr[pos];
		return arrBlau;
	}

	public static void drehen (int[]arr){
		drehen(arr,0);
	}
	private static void drehen(int[] arr, int pos) {
		if (pos >= arr.length)
			return;
		int temp = arr[pos];
		drehen(arr, pos + 1);
		arr[arr.length - 1 - pos] = temp;

	}
}
