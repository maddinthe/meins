package januar_15;

public class QuickSort {

	public static void main(String[] args) {
		String s = "UBPGATNS";
		System.out.println(qSort(s));
		char[] arr = { 'U', 'B', 'P', 'G', 'A', 'T', 'N', 'S' };
		//arr = qSortAr(arr);
		quickSort(arr);
		System.out.println(arr);
	}

	public static String qSort(String s) {
		if (s.length() < 2)
			return s;
		char pivot = s.charAt(0);
		String links = "";
		String rechts = "";
		for (int i = 1; i < s.length(); i++)
			if (pivot < s.charAt(i))
				rechts += s.charAt(i);
			else
				links += s.charAt(i);

		return qSort(links) + pivot + qSort(rechts);

	}

	public static char[] qSortAr(char[] arr) {
		char pivot = arr[0];
		char[] blau = new char[arr.length];
		int l = 0;
		int r = arr.length - 1;
		for (int i = 1; r > l; i++)
			if (pivot < arr[i])
				blau[r--] = arr[i];
			else
				blau[l++] = arr[i];
		blau[l] = pivot;

		if (l > 2) {
			char[] links = new char[l];
			for (int i = 0; i < l; i++)
				links[i] = blau[i];
			links = qSortAr(links);
			for (int i = 0; i < l; i++)
				blau[i] = links[i];
		}

		if (r < blau.length - 2) {
			r++;
			char[] rechts = new char[blau.length - r];
			for (int i = r; i < blau.length; i++)
				rechts[i - r] = blau[i];
			rechts = qSortAr(rechts);
			for (int i = r; i < blau.length; i++)
				blau[i] = rechts[i - r];
		}
		return blau;
	}

	public static void quickSort(char[] arr){
		quickSort(arr,0,arr.length-1);
	}
	private static void quickSort(char[] arr, int l, int r) {
		int i = l;
		int j = r;
		char pivot = arr[((l + r) / 2)];
		while (i <= j) {
			while (arr[i] < pivot)
				i++;
			while (arr[j] > pivot)
				j--;
			if (i <= j) {
				char tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;
				i++;
				j--;
			}
		}
		if(j>l)quickSort(arr,l,j);
		if(i<r)quickSort(arr,i,r);
	}

}
