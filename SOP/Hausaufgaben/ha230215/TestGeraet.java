package ha230215;

public class TestGeraet {

	public static void main(String[] args) {
		Router r=new Router();
		r.addRoute("192.168.0.0,255.255.0.0,137.193.7.89,3");  
		r.addRoute("192.168.20.0,255.255.254.0,137.193.80.70,3");
		r.addRoute("128.0.0.0,128.0.0.0,127.203.75.8,2");
		System.out.println(r);
		
		System.out.println(r.route(new IPAdresse("192.168.0.1")));
		System.out.println(r.route(new IPAdresse("193.168.0.1")));
	}

}
