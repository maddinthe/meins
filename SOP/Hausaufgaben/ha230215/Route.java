package ha230215;

public class Route {
	private Netzwerk net;
	private IPAdresse gw;
	private int intFace;
	
	
	public Route(IPAdresse netid,IPAdresse mask, IPAdresse gw, int intFace){
		this.net=new Netzwerk(netid, mask);
		this.gw=gw;
		this.intFace=intFace;
	}
	public Route(String netid, String mask, String gw, String intFace){
		this(new IPAdresse(netid),new IPAdresse(mask),new IPAdresse(gw),Integer.parseInt(intFace));
	}

	public IPAdresse getGw() {
		return gw;
	}

	public int getIntFace() {
		return intFace;
	}
	
	public int matcher(IPAdresse ziel){
		if(net.isInNet(ziel))
			return net.maskLength();
		return -1;
	}
	public String toString(){
		return net.getIP()+", "+net.getMask()+", "+gw+", "+intFace;
	}

	
	
}
