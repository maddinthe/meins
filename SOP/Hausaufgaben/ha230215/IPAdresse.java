package ha230215;


public class IPAdresse {
	private int adresse;
	
	public IPAdresse(int first, int second, int third, int fourth) {
		adresse = (first<<24) + (second<<16) + (third<<8)  + fourth;
		
	}

	public IPAdresse(int[] addr) {
		this(addr[0], addr[1], addr[2], addr[3]);
	}

	public IPAdresse(int adresse) {
		this.adresse = adresse;
	}
	
	public IPAdresse(String adresse){
		String[] arr=adresse.split("[^1234567890]",4);
		int[] addr=new int[4];
		for (int i=0;i<arr.length;i++){
			addr[i]=Integer.parseInt(arr[i]);
		}
		this.adresse=(addr[0]<<24)+(addr[1]<<16)+(addr[2]<<8)+(addr[3]);
	
	}

	public String toString(){
		return get(0)+"."+get(1)+"."+get(2)+"."+get(3);
	}
	public int get(int octett) {
		int ausg =adresse;
		return (ausg>>>(8*(3-octett)))&255;		
	}

	public void set(int octett, int inhalt) {
		if (inhalt<0||inhalt>255) return;
		int multi=3-octett;
		adresse=(adresse&(~(255<<(8*multi))))+(inhalt<<(8*multi));
	}
	
	public int toInt() {
		return adresse;
		
	}
	public char klasse(){
		int first= get(0);
		if (first<128)return 'a';
		if (first<192) return 'b';
		else{ 
			if(first<224)return 'c';
			if(first>239) return'e';
			else return 'd';
		}
		
	}
}
