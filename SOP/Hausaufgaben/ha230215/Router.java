package ha230215;

public class Router {
	private RListe Routingtabelle = new RListe();
	private boolean locked = false;

	public boolean isLocked() {
		return locked;
	}

	public void addRoute(String s) {
		String[] eintraege = s.split("[\\,]");
		Routingtabelle.add(new Route(eintraege[0], eintraege[1], eintraege[2],
				eintraege[3]));
	}

	public String toString() {
		String ausgabe = "";
		RListe.aufzaehler aufz = Routingtabelle.new aufzaehler();
		while (!aufz.isEnde()) {
			ausgabe += aufz.gibNaechsten() + "\n";
		}

		return ausgabe;
	}

	public String route(IPAdresse ip) {
		locked = true;
		RListe.aufzaehler aufz = Routingtabelle.new aufzaehler();
		Route best = aufz.gibNaechsten();
		while (!aufz.isEnde()) {
			Route r = aufz.gibNaechsten();
			if (r.matcher(ip) > best.matcher(ip))
				best = r;
		}
		if (best.matcher(ip) < 0)
			return "keine Route verf�gbar";

		return "�ber " + best.getIntFace() + " an " + best.getGw();
	}

	private class RListe {
		private Element first;

		public boolean isEmpty() {
			return first == null;
		}

		public class aufzaehler {
			private Element current = first;

			public boolean isEnde() {
				return current == null;
			}

			public Route gibNaechsten() {
				Route ret = current.value;
				current = current.next;
				return ret;
			}
		}

		public void add(Route value) {
			if (isEmpty())
				first = new Element(value);
			Element ele = new Element(value);
			ele.next = first;
			first = ele;

		}

		private class Element {
			private Route value;

			private Element next;

			public Element(Route value) {
				this.value = value;
			}

		}
	}
}
