package ha230215;

public class Netzwerk {
private IPAdresse ip;
private IPAdresse mask;

public Netzwerk(IPAdresse netid,IPAdresse mask){
	this.ip=netid;
	this.mask=mask;
}
public Netzwerk(IPAdresse netz){
	ip=netz;
	if (netz.get(0)<128) mask=new IPAdresse(-16777216);
	else if (netz.get(0)>=192) mask=new IPAdresse(-256);
	else mask=new IPAdresse(-65536);

}
public IPAdresse getNetzwerkIP() {
	return new IPAdresse(mask.toInt()&ip.toInt());
}
public IPAdresse getIP(){
	return ip;
}
public IPAdresse getMask() {
	return mask;
}

public IPAdresse getBroadcast(){
	return new IPAdresse(~mask.toInt()|ip.toInt());
}
public int maxClients(){
	return ~mask.toInt()-1;
}

public boolean isInNet(IPAdresse ip){
	return (mask.toInt()&this.ip.toInt())==(ip.toInt()&mask.toInt());
}
public int maskLength(){
	int i=0;
	for (int mask=this.mask.toInt();mask!=0;i++)
		mask<<=1;
	return i;
}

public IPAdresse defaultMask(){
	if (ip.get(0)<128)return new IPAdresse(-16777216);
	else if (ip.get(0)>=192) return new IPAdresse(-256);
	else return new IPAdresse(-65536);
}
public Netzwerk[] subNet(int clients, int subs) {
	int diff;
	if (clients == 0)
		diff = 32 - Integer.numberOfLeadingZeros(subs-1);
	else
		diff = (Integer.numberOfLeadingZeros(clients + 1)+ Integer.numberOfTrailingZeros(getMask().toInt()))-32;
	int nr=(int)Math.pow(2, diff);
	Netzwerk[] netze = new Netzwerk[nr];
	int mask=getMask().toInt()>>diff;
	Netzwerk temp=new Netzwerk(getNetzwerkIP(),new IPAdresse(mask));
	int ip=getNetzwerkIP().toInt();
	int Schrittweite=temp.maxClients()+2;
	for (int i = 0; i < netze.length; i++) {
		netze[i] = new Netzwerk(new IPAdresse(ip), new IPAdresse(mask));
		ip+=Schrittweite;
	}
	return netze;

}


}
