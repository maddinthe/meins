package ha190115;
public class Liste {
	private Element first;

	public boolean isEmpty() {
		return first == null;
	}

	public void add(String value) {
		if (first != null) {
			Element next = first;
			first = new Element(value);
			first.setNext(next);
		} else
			first = new Element(value);
	}

	public String toString() {
		if (first == null)
			return "[]";

		return first.toString("[");
	}

	public int getSize2() {
		return this.getSize2(first);
	}

	private int getSize2(Element el) {
		if (el == null)
			return 0;
		return this.getSize2(el.getNext()) + 1;

	}

	public int getSize() {
		if (first == null)
			return 0;
		return first.size();
	}

	public void delete(String value) {
		if (isEmpty())
			return;
		first = first.delete(value);
	}

	public void push(String arg0) {
		this.add(arg0);
	}

	public String pop() {
		if (isEmpty())
			return null;
		String tmp = first.getValue();
		first = first.getNext();
		return tmp;
	}

	public int getIndex(String value) {
		int size = getSize();
		if (isEmpty())
			return -1;
		if (first.getValue().equals(value))
			return size - 1;
		return size - first.getIndex(value);
	}

	public String atIndex(int i) {
		if (isEmpty())
			return null;
		int size = this.getSize();
		if (i == size - 1)
			return first.getValue();
		else {
			return first.atIndex(i - this.getSize() + 1);

		}
	}

	public String remove(int index) {
		int maxIndex = getSize() - 1;
		if (maxIndex - index < 0)
			return null;
		if (index == maxIndex) {
			String temp = first.getValue();
			first = first.getNext();
			return temp;
		}
		return remove(first, maxIndex - index);
	}

	private String remove(Element ele, int zahl) {
		if (zahl == 1) {
			String temp = ele.getNext().getValue();
			ele.setNext(ele.getNext().getNext());
			return temp;
		}
		return remove(ele.getNext(), zahl - 1);
	}

	public void insert(int index, String arg) {
		int size = getSize();
		if (index > size - 1)
			return;
		insert(arg,size - index- 1,first);
	}
	private void insert(String arg,int i,Element ele){
		if (i==0){
			Element tmp=ele.getNext();
			ele.setNext(new Element(arg));
			ele.getNext().setNext(tmp);
			return;
		}insert(arg,i-1,ele.getNext());
	}
}
