package ha190115;

public class Element {
	private String value;
	private Element next=null;

	public Element(String value) {
		super();
		this.value = value;
	}
	public String toString(String str){
		if (next==null)
			return (str+value+"]");
		return next.toString(str+value+", ");
	}
	public int size(){
		if (next==null)
			return 1;
		return next.size()+1;
	}
	public Element delete (String str){
		if (value.equals(str)) return next;
		if (next!=null)next=next.delete(str);
		return this;	
	}
	public int getIndex(String str){
		if (value.equals(str))
			return 0;
		return next.getIndex(str)+1;
		}
	public String atIndex(int i){
		if (i==0) return value;
		return next.atIndex(i+1);
	}
	
	public void setNext(Element next) {
		this.next = next;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Element getNext() {
		return next;
	}

}
