package ha020315;

public class TestSystem {
	private Liste prozesse = new Liste();
	private Liste pages = new Liste();
	int speicher = 1000;
	int auslastung = 0;
	int seitenwechsel=0;

	public TestSystem(int proze) {
		while (proze > 0) {
			prozesse.add(new Prozess(proze));
			proze--;
		}

	}
	public TestSystem(int proze,int ram){
		this(proze);
		speicher=ram;
	}
	public TestSystem() {
		this((int) (Math.random() * 10));
	}

	
	public int starten(){
		while(!prozesse.isEmpty()){
			for(Object p:prozesse){
				Liste pPages=((Prozess)p).ausfuehren();
				for(Object pa:pPages){
					if (!((Page)pa).isInRam()){
						pageLaden((Page)pa);
						((Prozess)p).sWechsel++;
					}
				}
			}	
		}
		return seitenwechsel;		
	}

	public void prozessBeenden(Prozess p){
		System.out.println(p+" wurde beendet");
		prozesse.delete(p);
		
	}
	
 //<<---------------- Seiten Laden -------------- 
	public void pageLaden(Page p){
		if (auslastung==speicher){
			for(Object pa:pages){
				if(((Page)pa).isRef()){
					Page po=(Page)pages.pop();
					po.setRef(false);
					pages.insert(0,po);
				}else {((Page)pages.pop()).setInRam(false);;
				pages.insert(0, p);
				p.setInRam(true);}
				break;
			}		
		}else {pages.add(p);
			auslastung++;}
		seitenwechsel++;
		
	}
	
//<<---------------- Subklasse Prozess --------------------	
	public class Prozess {
		private Page[] pages;
		private int counter=1000;
		private int id=0;
		private int sWechsel=0;
		
		
		public Prozess(int nrOfPages,int id){
			this.pages=new Page[nrOfPages];
			for (int i=0;i<pages.length;i++){
				this.pages[i]=new Page();
			}
			this.id=id;
			System.out.println(this+" hat "+this.pages.length+" Pages");
			
		}
		public Prozess(){
			this((int) (Math.random()*100)+1,(int) (Math.random()*100)+1);		
		}
		public Prozess(int id){
			this((int) (Math.random()*100)+1,id);

		}
		public String toString(){
			return "Prozess "+ id+", Seitenwechsel: "+sWechsel;
		}
		
		public Liste ausfuehren(){
			Liste ret=new Liste();
			for (Page p:pages){
				boolean ref=Math.random()<0.125;
				if (ref==true){
					p.setRef(ref);
					ret.add(p);
				}
			}
			counter--;
			if (counter==0)
				TestSystem.this.prozessBeenden(this);
			return ret;
		}
		public boolean isFinished(){
			return counter==0;
		}
		
		
		
	}
	
}
