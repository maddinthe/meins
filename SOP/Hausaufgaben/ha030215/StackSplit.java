package ha030215;

public class StackSplit {

	public static void main(String[] args) {
		String[] sa1 = split("Hallo sch�ne Welt!", '�');
		String[] sa2 = splitB("Hallo sch�ne Welt!", '�');
		for (String a : sa1)
			System.out.println(a);
		for (String a : sa2)
			System.out.println(a);
		

	}

	public static String[] split(String s, char c) {
		if (s.length() == 0)
			return new String[0];
		if (s.charAt(0)==c)
			return split(s.substring(1),c,0);
		if (s.charAt(s.length()-1)==c)
			return split(s.substring(0,s.length()-1), c,0);			
		return split(s, c, 0);

	}

	private static String[] split(String s, char c, int i) {
		if (s.length() == 0)
			return new String[i + 1];
		if (s.charAt(0) == c)
			return split(s.substring(1), c, i+1);
		else {
			String[] sa = split(s.substring(1), c, i);
			if (sa[i] == null)
				sa[i] = s.charAt(0) + "";
			else
				sa[i] = s.charAt(0) + sa[i];
			return sa;
		}

	}
	
	public static String[] splitB(String s,char c){
		if (s.charAt(0)==c)
			return splitB(s.substring(1),c,0);
		if (s.charAt(s.length()-1)==c)
			return splitB(s.substring(0,s.length()-1), c,0);
		return splitB(s,c,0);
	}
	
	
	private static String[] splitB(String s, char c, int i){
		if (s.contains(c+"")){
			String[] sa=splitB(s.substring(s.indexOf(c)+1), c, i+1);
			sa[i]=s.substring(0, s.indexOf(c));
			return sa;
			}
		else {
			String[] sa = new String[i+1];
			sa[i]=s;
			return sa;
		}
		
	}

}
