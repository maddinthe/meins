package ha100215;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Component;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ButtonGroup;
import javax.swing.JTextPane;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import java.awt.event.ActionListener;
import javax.swing.JScrollPane;
import java.awt.Font;

public class GrOberFl extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private int[] ip={1,0,0,0};
	private boolean subAuto=false;
	private char weiter='a';
	private int nummer=1;
	private String subnet="128.0.0.0\t  /1";
	private final Action action = new SwingAction();
	private final Action action_1 = new SwingAction_1();
	private final Action action_2 = new SwingAction_2();
	private final Action action_3 = new SwingAction_3();
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GrOberFl frame = new GrOberFl();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public GrOberFl() {
		setTitle("Maddins Subnetzrechner");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 599);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JCheckBox rdbtnStandardsubnetz = new JCheckBox("Standard Subnetz");
		rdbtnStandardsubnetz.setFont(new Font("Arial", Font.PLAIN, 11));
		rdbtnStandardsubnetz.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JCheckBox jc=(JCheckBox) e.getSource();
				subAuto=jc.isSelected();
			}
		});
		rdbtnStandardsubnetz.setBounds(315, 41, 113, 23);
		contentPane.add(rdbtnStandardsubnetz);
		
		JLabel lblIpadresse = new JLabel("IP-Adresse");
		lblIpadresse.setFont(new Font("Arial", Font.PLAIN, 11));
		lblIpadresse.setBounds(10, 14, 63, 14);
		contentPane.add(lblIpadresse);
		
		JLabel lblNewLabel = new JLabel("Subnetzmaske");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 11));
		lblNewLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		lblNewLabel.setBounds(10, 45, 83, 14);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 97, 414, 453);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setEditable(false);
		
		JSpinner spi_octett1 = new JSpinner();
		spi_octett1.setFont(new Font("Arial", Font.PLAIN, 11));
		spi_octett1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSpinner ms=(JSpinner) arg0.getSource();
				ip[0]=(int)(ms.getValue());
				
			}
		});
		spi_octett1.setModel(new SpinnerNumberModel(1, 1, 255, 1));
		spi_octett1.setBounds(96, 11, 46, 20);
		contentPane.add(spi_octett1);
		
		JSpinner spi_octett2 = new JSpinner();
		spi_octett2.setFont(new Font("Arial", Font.PLAIN, 11));
		spi_octett2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSpinner ms=(JSpinner) arg0.getSource();
				ip[1]=(int)(ms.getValue());
			}
		});
		spi_octett2.setModel(new SpinnerNumberModel(0, 0, 255, 1));
		spi_octett2.setBounds(148, 11, 46, 20);
		contentPane.add(spi_octett2);
		
		JSpinner spi_octett3 = new JSpinner();
		spi_octett3.setFont(new Font("Arial", Font.PLAIN, 11));
		spi_octett3.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSpinner ms=(JSpinner) arg0.getSource();
				ip[2]=(int)(ms.getValue());
			}
		});
		spi_octett3.setModel(new SpinnerNumberModel(0, 0, 255, 1));
		spi_octett3.setBounds(201, 11, 46, 20);
		contentPane.add(spi_octett3);
		
		JSpinner spi_octett4 = new JSpinner();
		spi_octett4.setFont(new Font("Arial", Font.PLAIN, 11));
		spi_octett4.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSpinner ms=(JSpinner) arg0.getSource();
				ip[3]=(int)(ms.getValue());
			}
		});
		spi_octett4.setModel(new SpinnerNumberModel(0, 0, 255, 1));
		spi_octett4.setBounds(254, 11, 46, 20);
		contentPane.add(spi_octett4);
		
		JButton btn_berechnung = new JButton("Berechne");
		btn_berechnung.setFont(new Font("Arial", Font.PLAIN, 11));
		btn_berechnung.setAction(action);
		btn_berechnung.setActionCommand("berechne");
		btn_berechnung.setBounds(315, 11, 113, 23);
		contentPane.add(btn_berechnung);
		
		JComboBox cb_maskAuswahl = new JComboBox();
		cb_maskAuswahl.setFont(new Font("Arial", Font.PLAIN, 11));
		cb_maskAuswahl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				subnet=(String) ((JComboBox)arg0.getSource()).getSelectedItem();
			}
		});
		cb_maskAuswahl.setModel(new DefaultComboBoxModel(new String[] {"128.0.0.0\t /1", "192.0.0.0\t /2", "224.0.0.0\t /3", "240.0.0.0\t /4", "248.0.0.0\t /5", "252.0.0.0\t /6", "254.0.0.0\t /7", "255.0.0.0\t /8", "255.128.0.0\t /9", "255.192.0.0\t /10", "255.224.0.0\t /11", "255.240.0.0\t /12", "255.248.0.0\t /13", "255.252.0.0\t /14", "255.254.0.0\t /15", "255.255.0.0\t /16", "255.255.128.0\t /17", "255.255.192.0\t /18", "255.255.224.0\t /19", "255.255.240.0\t /20", "255.255.248.0\t /21", "255.255.252.0\t /22", "255.255.254.0\t /23", "255.255.255.0\t /24", "255.255.255.128\t /25", "255.255.255.192\t /26", "255.255.255.224\t /27", "255.255.255.240\t /28", "255.255.255.248\t /29", "255.255.255.252\t /30", "255.255.255.254\t /31", "255.255.255.255\t /32"}));
		cb_maskAuswahl.setBounds(96, 42, 201, 20);
		contentPane.add(cb_maskAuswahl);
		
		JRadioButton keinSubnetting = new JRadioButton("keine");
		keinSubnetting.setFont(new Font("Arial", Font.PLAIN, 11));
		keinSubnetting.setAction(action_1);
		keinSubnetting.setSelected(true);
		buttonGroup.add(keinSubnetting);
		keinSubnetting.setBounds(124, 67, 53, 23);
		keinSubnetting.setActionCommand("keine");
		contentPane.add(keinSubnetting);
		
		JRadioButton subnettingNetze = new JRadioButton("Netze");
		subnettingNetze.setFont(new Font("Arial", Font.PLAIN, 11));
		buttonGroup.add(subnettingNetze);
		subnettingNetze.setAction(action_3);
		subnettingNetze.setActionCommand("Netze");		
		subnettingNetze.setBounds(234, 67, 63, 23);
		contentPane.add(subnettingNetze);
		
		JRadioButton subnettingHosts = new JRadioButton("Hosts");
		subnettingHosts.setFont(new Font("Arial", Font.PLAIN, 11));
		buttonGroup.add(subnettingHosts);
		subnettingHosts.setAction(action_2);
		subnettingHosts.setBounds(179, 67, 53, 23);
		subnettingHosts.setActionCommand("Hosts");
		contentPane.add(subnettingHosts);
		
		JTextPane txtpnWeitereUnterteilung = new JTextPane();
		txtpnWeitereUnterteilung.setFont(new Font("Arial", Font.PLAIN, 11));
		txtpnWeitereUnterteilung.setOpaque(false);
		txtpnWeitereUnterteilung.setText("Weitere Unterteilung");
		txtpnWeitereUnterteilung.setBounds(10, 70, 113, 20);
		contentPane.add(txtpnWeitereUnterteilung);
		
		JSpinner spinner = new JSpinner();
		spinner.setFont(new Font("Arial", Font.PLAIN, 11));
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				nummer=(int) ((JSpinner) e.getSource()).getValue();
			}
		});
		spinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		spinner.setBounds(315, 71, 109, 20);
		contentPane.add(spinner);
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(ACTION_COMMAND_KEY, "berechne");
			putValue(NAME, "Berechne");
			putValue(SHORT_DESCRIPTION, "Berechnet alles nach den angegebenen daten");
		}
		public void actionPerformed(ActionEvent e) {
			
			if ("berechne".equals(e.getActionCommand())){
				Netzwerk net;
				String str;
				Netzwerk[] netArr=new Netzwerk[0];
				if(subAuto)
				net=new Netzwerk(new IPAdresse(ip));
				else{
					int[] sub=new int[4];
					String[] substr=subnet.split("[^1234567890]");
					for(int i=0;i<4;i++)
						sub[i]=Integer.parseInt(substr[i]);
					net=new Netzwerk(new IPAdresse(ip),new IPAdresse(sub));
				}
				str="IPadresse: "+net.getIP()+" Subnetzmaske: "+net.getMask()+"\n"+
						"NetId: "+net.getNetzwerkIP()+" Broadcast: "+net.getBroadcast()+"\n"
						+"Hosts in diesem Netz:"+net.maxClients()+"\n";
				if (weiter=='b')
					netArr=net.subNet(nummer, 0);
				if (weiter=='c')
					netArr=net.subNet(0, nummer);
				if (weiter!='a'){
					str+="Neue Subnetzmaske: "+netArr[0].getMask()+" Hosts pro Netz: "+netArr[0].maxClients()+"\n";
					for (int i=0;i<netArr.length;i++){
						str+=(i+1)+". Netz: "+netArr[i].getIP()+" BC: "+netArr[i].getBroadcast()+"\n";
						
					}
					
				}
				
				textArea.setText(str);
				}
			}
		}
	
	
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "Keine");
			putValue(SHORT_DESCRIPTION, "keine Weitere Unterteilung");
		}
		public void actionPerformed(ActionEvent e) {
			weiter='a';
			
			
		}
	}
	private class SwingAction_2 extends AbstractAction {
		public SwingAction_2() {
			putValue(NAME, "Hosts");
			putValue(SHORT_DESCRIPTION, "Berechnung nach Anzahl Hosts");
		}
		public void actionPerformed(ActionEvent e) {
			weiter='b';
		}
	}
	private class SwingAction_3 extends AbstractAction {
		public SwingAction_3() {
			putValue(NAME, "Netze");
			putValue(SHORT_DESCRIPTION, "Berechnung nach Anzahl Netze");
		}
		public void actionPerformed(ActionEvent e) {
			weiter='c';
		}
	}
}
