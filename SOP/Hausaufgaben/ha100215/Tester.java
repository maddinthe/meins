package ha100215;

public class Tester {

	public static void main(String[] args) {
		IPAdresse ip=new IPAdresse(1,2,3,4);
		System.out.println(ip.get(0));
		System.out.println(ip.get(1));
		System.out.println(ip.get(2));
		System.out.println(ip.get(3));
		
		ip.set(0, 44);
		ip.set(1, 33);
		ip.set(2, 22);
		ip.set(3, 11);
		System.out.println(ip.get(0));
		System.out.println(ip.get(1));
		System.out.println(ip.get(2));
		System.out.println(ip.get(3));
		
	}

}
