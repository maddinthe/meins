package ha200415;

import java.util.Iterator;

public class HashTabelle2 <T> implements Iterable<T> {
	private T[] eimer;
	private int fuellstand;
	
	@SuppressWarnings("unchecked")
	public HashTabelle2 (int laenge){
		this.eimer=(T[]) new Object[laenge];
	}
	
	
	public void add(T value){
		int nr=(value.hashCode()&0x7fffffff)%eimer.length;
		int stop=nr;
		while (true){
			if (eimer[nr]==null){
				eimer[nr]=value;
				fuellstand+=1d/eimer.length*100;
				if (fuellstand>=70){
					HashTabelle2<T> neu=new HashTabelle2<>(eimer.length*2);
					for (T temp:this)
						neu.add(temp);
					fuellstand=neu.fuellstand;
					eimer=neu.eimer;
					
				}
				return;
			}
			nr++;
			if (nr>=eimer.length) nr=0;
			if (stop==nr)return;
						
		}
		
	}

	public boolean contains(T value){
		int nr=(value.hashCode()&0x7fffffff)%eimer.length;
		int stop=nr;
		while (true){
			if (eimer[nr]!=null&&eimer[nr].equals(value)) return true;
			nr++;
			if (nr>=eimer.length) nr=0;
			if (nr==stop) return false;
			
		}
	}

	public Iterator<T> iterator() {
		
		return new Iterator<T>(){
			private int index=0;
			public boolean hasNext() {
				while (index<eimer.length&&eimer[index]==null)
					index++;
				return index<eimer.length;
			}

			
			public T next() {
				return eimer[index++];
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
			
		};
	}
	
	
	
}
