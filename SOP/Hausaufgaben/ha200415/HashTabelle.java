package ha200415;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

public class HashTabelle<T> implements Iterable<T> {
	private Liste<T>[] eimer;
	private int fuellstand;

	@SuppressWarnings("unchecked")
	public HashTabelle(int groesse) {
		eimer = new Liste[groesse];
	}

	public void add(T value) {
		int nr = (value.hashCode() & 0x7fffffff) % eimer.length;
		if (eimer[nr] == null)
			eimer[nr] = new Liste<T>();
		if (!eimer[nr].contains(value)) {
			eimer[nr].add(value);
			fuellstand += (1d / eimer.length * 100);
		}
		if (fuellstand >= 70)
			{
			HashTabelle<T> neu = new HashTabelle<>(eimer.length * 2);
			for (T temp : this)
				neu.add(temp);
			fuellstand = neu.fuellstand;
			eimer = neu.eimer;
			}
	}


	public boolean contains(T value) {
		int nr = (value.hashCode() & 0x7fffffff) % eimer.length;
		if (eimer[nr] == null)
			return false;
		if (eimer[nr].contains(value))
			return true;
		return false;
	}

	public Iterator<T> iterator() {
		return new Iterator<T>() {

			private int index = 0;
			HashTabelle<T>.Liste<T>.Element current;

			public boolean hasNext() {
				if (current == null) {
					while (index < eimer.length
							&& (eimer[index] == null || eimer[index].isEmpty()))
						index++;
					current = eimer[index].first;
					return true;
				}
				if (current.next == null) {
					index++;
					while (index < eimer.length
							&& (eimer[index] == null || eimer[index].isEmpty()))
						index++;
					if (index < eimer.length) {
						current = eimer[index].first;
						return true;
					}
				} else {
					current = current.next;
					return true;
				}
				return false;
			}

			public T next() {
				return current.value;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub

			}

		};
	}

	public class Liste<E> implements Collection<E> {

		private Element first;
		private int size = 0;

		public String toString() {
			String ret = "[";
			for (E temp : this)
				ret += temp + ", ";
			ret = (ret.length() == 1) ? (ret += "]") : (ret = ret.substring(0,
					ret.length() - 2) + "]");
			return ret;
		}

		public E pop() {
			E ret = first.value;
			first = first.next;
			size--;
			return ret;
		}

		public boolean add(E value) {
			if (isEmpty())
				first = new Element(value);
			else {
				Element temp = new Element(value);
				temp.next = first;
				first = temp;
			}
			size++;
			return true;
		}

		public boolean addAll(Collection<? extends E> c) {
			for (E temp : c) {
				add(temp);
			}
			return true;
		}

		public void clear() {
			first = null;
			size = 0;

		}

		public boolean contains(Object o) {
			for (E temp : this)
				if (o.equals(temp))
					return true;
			return false;
		}

		public boolean containsAll(Collection<?> c) {
			for (Object obj : c)
				if (!this.contains(obj))
					return false;
			return true;
		}

		public boolean isEmpty() {
			return first == null;
		}

		public Iterator<E> iterator() {
			return new Aufzaehler();
		}

		public boolean remove(Object o) {
			if (first.value.equals(o)) {
				first = first.next;
				size--;
				return true;
			}
			for (Element temp = first; temp.next != null; temp = temp.next)
				if (temp.next.value.equals(o)) {
					temp.next = temp.next.next;
					size--;
					return true;
				}
			return false;
		}

		public boolean removeAll(Collection<?> c) {
			boolean ret = false;
			for (Object o : c)
				while (this.contains(o)) {
					remove(o);
					ret = true;
				}
			return ret;
		}

		public boolean retainAll(Collection<?> c) {
			Liste<E> l = new Liste<>();
			for (E temp : this)
				if (c.contains(temp))
					l.addEnd(temp);
			boolean ret = l.size() == this.size();
			first = l.first;
			size = l.size;
			return !ret;
		}

		public int size() {
			return size;
		}

		public Object[] toArray() {
			Object[] ret = new Object[size()];
			int i = 0;
			for (E temp : this)
				ret[i++] = temp;
			return ret;
		}

		public void addEnd(E value) {
			if (first == null)
				first = new Element(value);
			else {
				Element temp = first;
				for (; temp.next != null; temp = temp.next)
					;
				temp.next = new Element(value);
			}
			size++;
		}

		@SuppressWarnings({ "unchecked", "hiding" })
		public <T> T[] toArray(T[] a) {
			if (size > a.length)
				return (T[]) Arrays.copyOf(toArray(), size, a.getClass());
			System.arraycopy(toArray(), 0, a, 0, size);
			if (a.length > size)
				a[size] = null;
			return a;

		}

		public void sort() {
			first = sort(this).first;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		private Liste<E> sort(Liste<E> list) {
			if (list.first == null || list.first.next == null)
				return list;
			Liste<E> links = new Liste<>();
			Liste<E> rechts = new Liste<>();
			E pivot = list.pop();
			for (E temp : list)
				if (((Comparable) pivot).compareTo(temp) < 0)
					rechts.add(temp);
				else
					links.add(temp);
			rechts.sort();
			links.sort();

			links.addEnd(pivot);
			for (E temp : rechts)
				links.addEnd(temp);
			return links;
		}

		public void sort(Comparator<E> comp) {
			first = sort(comp, this).first;
		}

		private Liste<E> sort(Comparator<E> comp, Liste<E> list) {
			if (list.first == null || list.first.next == null)
				return list;
			Liste<E> links = new Liste<>();
			Liste<E> rechts = new Liste<>();
			E pivot = list.pop();
			for (E temp : list)
				if (comp.compare(pivot, temp) < 0)
					rechts.add(temp);
				else
					links.add(temp);
			rechts.sort(comp);
			links.sort(comp);

			links.addEnd(pivot);
			for (E temp : rechts)
				links.addEnd(temp);
			return links;
		}

		private class Aufzaehler implements Iterator<E> {
			private Element current = first;

			public boolean hasNext() {
				return current != null;
			}

			@Override
			public void remove() {
			}

			public E next() {
				E ret = (E) current.value;
				current = current.next;
				return ret;

			}

		}

		private class Element {
			private E value;
			private Element next;

			public Element(E value) {
				this.value = value;
			}

		}

	}

}
