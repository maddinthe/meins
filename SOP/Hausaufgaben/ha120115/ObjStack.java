package ha120115;

public class ObjStack {
	private Object[] objArr = new Object[0];

	public boolean isEmpty() {
		return objArr.length == 0;
	}

	public void push(Object in) {
		Object[] newArr = new Object[objArr.length + 1];
		for (int i = 0; i < objArr.length; i++)
			newArr[i + 1] = objArr[i];
		newArr[0] = in;
		objArr = newArr;
	}
	public Object pop(){
		Object ret=objArr[0];
		Object[] newArr=new Object[objArr.length-1];
		for (int i=0;i<newArr.length;i++)
			newArr[i]=objArr[i+1];
		objArr=newArr;
		return ret;
	}
}
