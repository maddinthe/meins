package ha120115;
public class Stack {
	private Object[] objArr = new Object[2];

	public boolean isEmpty() {
		return objArr[0] == null;
	}

	public void push(Object in) {
		if (objArr[0] == null) objArr[0] = in;
		else {
			Object[] temp= {in,objArr};
			objArr= temp;
		}
	}

	public Object pop() {
		Object temp = objArr[0];
		if (objArr[1] != null) objArr = (Object[]) objArr[1];
		else objArr[0] = null;
		return temp;
	}
}
