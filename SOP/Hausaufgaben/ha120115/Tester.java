package ha120115;

public class Tester {

	public static void main(String[] args){
		// TODO Auto-generated method stub
		Stack s=new Stack();
		Runtime rt = Runtime.getRuntime();
		ObjStack os= new ObjStack();
		int[] test=new int[100000];
		for (int i=0;i<test.length;i++)
			test[i]=(int)Math.random()*10;
		
		
		System.out.println("Testbeginn: bef�llen");
		long mem = rt.totalMemory()-rt.freeMemory();
		long start=System.currentTimeMillis();
		for (int i:test)
			s.push(i);
		System.out.println("Laufzeit 1 bef�llen: "+ (System.currentTimeMillis()-start)+" ms");
		System.out.println("Speicherbedarf: "+((rt.totalMemory() - rt.freeMemory())-mem));
		
		
		mem = rt.totalMemory()-rt.freeMemory();
		start=System.currentTimeMillis();
		for (int i:test)
			os.push(i);
		System.out.println("Laufzeit 2 bef�llen: "+ (System.currentTimeMillis()-start)+" ms");
		System.out.println("Speicherbedarf: "+((rt.totalMemory() - rt.freeMemory())-mem));
		
		
		
		start=System.currentTimeMillis();	
		while (!s.isEmpty())
			s.pop();
		System.out.println("Laufzeit 1 leeren: "+ (System.currentTimeMillis()-start)+" ms");
		
		start=System.currentTimeMillis();
		while (!os.isEmpty())
			os.pop();
		System.out.println("Laufzeit 2 leeren: "+ (System.currentTimeMillis()-start)+" ms");
	}

}
