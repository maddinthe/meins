package ha230315;

import java.util.Comparator;
import java.util.Scanner;

public class Tester {

	public static void main(String[] args) {
		Liste<Auto> parkplatzDraussen=new Liste<>();
		Liste<Auto> autohaus=new Liste<>();
		Liste<Auto> lkw=new Liste<>();
		Scanner s=new Scanner(System.in);
		
		for (int i=5;i>0;i--)
			lkw.add(new Auto());
		Auto[] merkhilfe=lkw.toArray(new Auto[lkw.size()]);
		
		for (Auto a:lkw)
			System.out.println(a);
		
		for(Auto a:lkw)
			if (a.isCabrio())
				autohaus.add(a);
		
		s.nextLine();
		lkw.removeAll(autohaus);
		parkplatzDraussen.addAll(lkw);
		lkw.removeAll(parkplatzDraussen);
		
		System.out.println();
		System.out.println("Auf dem LKW sind folgende Autos:");
		if (lkw.isEmpty()) System.out.println("keine");
		else for (Auto a:lkw)
			System.out.println(a);
		System.out.println();
		
		s.nextLine();
		System.out.println("Auf dem Parkplatz sind folgende Autos:");
		parkplatzDraussen.sort();
		for (Auto a:parkplatzDraussen)
			System.out.println(a);
		System.out.println();
		
		s.nextLine();
		autohaus.sort();
		System.out.println("Im Autohaus sind folgende Autos:");
		for (Auto a:autohaus)
			System.out.println(a);
		
		s.nextLine();
		System.out.println("Folgendes Auto soll verkauft werden:");
		System.out.println(merkhilfe[4]);
		if (autohaus.contains(merkhilfe[4]))
			{
			System.out.println("Das gesuchte Auto steht im Autohaus");
			autohaus.remove(merkhilfe[4]);
			}
		else if (parkplatzDraussen.contains(merkhilfe[4])){
				System.out.println("Das gesuchte Auto steht draussen");
				parkplatzDraussen.remove(merkhilfe[4]);
				
		}
		
		System.out.println("Verkauft");
		
		s.nextLine();
		Liste<Auto> inventar=new Liste<>();
		inventar.addAll(autohaus);
		inventar.addAll(parkplatzDraussen);
		inventar.sort();
		System.out.println("Neues Inventar:");
		for (Auto a:inventar)
			System.out.println(a);
		
		s.nextLine();
		inventar.removeAll(autohaus);
		System.out.println("Draussen stehen folgende Autos:");
		inventar.sort(Auto.FARBE_AND_SNR_ORDER);
		for (Auto a:inventar)
			System.out.println(a);
		
		
		
	}
}
