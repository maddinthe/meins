package ha230315;

import java.util.Comparator;

public class Auto implements Comparable<Auto> {
	public static final Comparator<Auto> FARBE_AND_SNR_ORDER=new Comparator<Auto>(){
		public int compare(Auto o1, Auto o2) {
			if (o1.getFarbe().compareToIgnoreCase(o2.getFarbe())==0)
				return o1.getSerNr().compareToIgnoreCase(o2.getSerNr());
			return o1.getFarbe().compareToIgnoreCase(o2.getFarbe());
		}};
	private int hubraum;
	private String farbe;
	private String serNr;
	private boolean cabrio;
	

	public Auto() {
		hubraum =((int)(Math.random()*20))*100 + 1000;
		String[] farbe = { "blau", "rot", "gr�n", "wei�", "gelb" };
		this.farbe = farbe[(int) (Math.random() * farbe.length)];
		serNr = (char)(Math.random()*26+'A')+""+(char)(Math.random()*26+'A')+"-"+Long.toString(System.currentTimeMillis());
		cabrio=(Math.random()<0.5);
	}

	public int getHubraum() {
		return hubraum;
	}

	public String getFarbe() {
		return farbe;
	}

	public String getSerNr() {
		return serNr;
	}

	public boolean isCabrio() {
		return cabrio;
	}

	public int compareTo(Auto a) {
		return serNr.compareTo(a.serNr);
	}

	@Override
	public String toString() {
		return "Auto [Hubraum=" + hubraum + ", Farbe=" + farbe + ",\t SerNr="
				+ serNr + ", Cabrio=" + cabrio + "]";
	}

}
