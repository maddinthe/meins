package ha160315;

import java.util.Scanner;

public class Starter {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		while(true){
			System.out.println("Geben Sie einen Punkt ein, z.B (-3,4) (\"exit\" zum abbrechen):");
			String pkt=s.next();
			if (pkt.equals("exit")) break;
			System.out.println("Geben Sie jetzt einen zweiten Punkt oder einen Anstieg, z.B. -3/2 ein:");
			String pkt2=s.next();
			String []bla=pkt.substring(1, pkt.length()-1).split("[/,]");
			int[] pkt1={Integer.parseInt(bla[0]),Integer.parseInt(bla[1])};
			Plotter p=new Plotter(pkt1[0],pkt1[1],pkt2);
			System.out.println(p.zeichnen()+"\n<--------------------------------------------------------------------->");
		}

	}

}
