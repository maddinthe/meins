package ha160315;

public class Plotter {

	// private Punkt eins;
	private String gleichung = "g(x)=";
	private double m;
	private double b;
	private int punkt1X;
	private int punkt1Y;

	public Plotter(int p1x, int p1y, int p2x, int p2y) {
		punkt1X = p1x;
		punkt1Y = p1y;
		m = ((double) (p1y - p2y)) / (p1x - p2x);
		b = p1y - (m * p1x);
		Bruch mb = new Bruch(m);
		Bruch bb = new Bruch(b);
		if (b == 0)
			gleichung += mb + "x";
		else
			gleichung += mb + "x" + bb;

	}

	public Plotter(int p1x, int p1y, String steigung) {
		punkt1X = p1x;
		punkt1Y = p1y;
		if (steigung.contains("/") && !steigung.contains(",")) { // <--- pr�fung
																	// ob
																	// Steigung
																	// oder
																	// Punkt
			String[] temp = steigung.split("[///:]");
			if (temp.length == 1)
				m = Double.parseDouble(temp[0]);
			else
				m = Double.parseDouble(temp[0]) / Double.parseDouble(temp[1]);
		} else {
			String[] temp = steigung.substring(1, steigung.length() - 1).split(
					"[,]");
			m = (double) (p1y - Integer.parseInt(temp[1]))
					/ (p1x - Integer.parseInt(temp[0]));
		}
		b = p1y - (m * p1x);

		Bruch mb = new Bruch(m);
		Bruch bb = new Bruch(b);

		if (b == 0)
			gleichung += mb + "x";
		else
			gleichung += mb + "x" + bb;

	}

	// <----------------- Array Bauen ist die Ausgelagerte Funktion um das
	// Stadnardarray zu zeichen in dessen mitte der Punkt eins ist
	private char[][] arrayBauen() {
		char[][] print = new char[12][41];
		for (int i = 0; i < print.length - 2; i++)
			print[i][3] = '|';
		for (int i = 0; i < print[3].length - 4; i++)
			print[10][i + 4] = '-';

		print[0][3] = '^';
		print[0][2] = 'y';
		print[5][3] = '+';
		print[10][3] = '+';
		print[10][21] = '+';
		print[10][40] = '>';
		String zahlY = Integer.toString(punkt1Y);
		if (zahlY.charAt(0) == '-') {
			print[5][0] = '-';
			print[5][1] = zahlY.charAt(1);
			if (zahlY.length() > 2)
				print[5][2] = zahlY.charAt(2);
		} else {
			print[5][0] = zahlY.charAt(0);
			if (zahlY.length() > 1)
				print[5][1] = zahlY.charAt(1);
			if (zahlY.length() > 2)
				print[5][2] = zahlY.charAt(2);
		}
		String yScala = Integer.toString(punkt1Y - 5);
		print[10][0] = yScala.charAt(0);
		if (yScala.length() > 1)
			print[10][1] = yScala.charAt(1);
		if (yScala.length() > 2)
			print[10][2] = yScala.charAt(2);

		String x = Integer.toString(punkt1X);
		String xScala = Integer.toString(punkt1X - 9);

		if (xScala.charAt(0) == '-') {
			print[11][2] = '-';
			print[11][3] = xScala.charAt(1);
			if (xScala.length() > 2)
				print[11][4] = xScala.charAt(2);
		} else {
			print[11][2] = xScala.charAt(0);
			if (xScala.length() > 1)
				print[11][3] = xScala.charAt(1);
			if (xScala.length() > 2)
				print[11][4] = xScala.charAt(2);
		}

		if (x.charAt(0) == '-') {
			print[11][21] = '-';
			print[11][22] = x.charAt(1);
			if (x.length() > 2)
				print[11][23] = x.charAt(2);
		} else {
			print[11][21] = x.charAt(0);
			if (x.length() > 1)
				print[11][22] = x.charAt(1);
			if (x.length() > 2)
				print[11][23] = x.charAt(2);
		}
		print[5][21] = 'X';
		print[11][40] = 'x';

		return print;
	}

	// <<---------------- zeichnen baut einenen String aus dem 2D Char Array
	// welches von bauen kommet und setzt den rest der Funktionsdaten ein
	public String zeichnen() {
		String ret = "Die Gleichung lautet:" + gleichung + "\n";
		char[][] print = arrayBauen();

		int[] y = new int[37];

		// <---- berechnung der Y werte zu den X-Werten von eins.x-9,5 -
		// eins.x+9
		double x = punkt1X - 9.5;
		for (int i = y.length - 1; i >= 0; i--) {
			y[i] = (int) Math.round(m * x + b);
			x += 0.5;
		}

		// <---- Bestimmen der grenzwerte f�r unten und oben
		int ug = punkt1Y - 6;
		int og = punkt1Y + 7;

		// <---- bestimmtung des offsets vom kleinsten y zu 0
		int offset = punkt1Y - 5;

		// <---- x entlang der geraden einzeichen
		for (int i = 0; i < y.length; i++)
			if (y[i] < og && y[i] > ug)
				print[y[i] - offset][i + 4] = 'x';

		// <---- Char Array in den Returnstring umwandeln
		for (char[] i : print) {
			for (char j : i)
				ret += j;
			ret += "\n";
		}

		return ret;
	}
	private class Bruch {
		String bruch;
//		double zahl;

		public Bruch(double bruch) {
//			this.zahl = bruch;
			int nenner = (int) Math.pow(10, (double) nKom(bruch));
			int ggt = ggt((int) (bruch * nenner), nenner);
			if (ggt < 0)
				ggt = -ggt;
			if (nenner / ggt == 1)
				this.bruch = (int) bruch + "";
			else
				this.bruch = "(" + (int) (bruch * nenner / ggt) + "/" + nenner
						/ ggt + ")";
		}

//		public Bruch(String bruch) {
//			String[] temp = bruch.split("[^-0123456789]", 2);
//
//			int[] zahlen = { Integer.parseInt(temp[0]),
//					Integer.parseInt(temp[1]) };
//			int ggt = ggt(zahlen[0], zahlen[1]);
//			zahl = (double) zahlen[0] / zahlen[1];
//			if (ggt < 0)
//				ggt = -ggt;
//			if (zahlen[1] / ggt == 1)
//				this.bruch = "" + zahlen[0] / ggt;
//			else
//				this.bruch = "(" + zahlen[0] / ggt + "/" + zahlen[1] / ggt
//						+ ")";
//
//		}

//		public double toDouble() {
//			return zahl;
//		}

		public String toString() {
			return (bruch.charAt(1) != '-') ? "+" + bruch : "-("
					+ bruch.substring(2);
		}

		private int nKom(double zahl) {
			return (zahl % 1 == 0) ? 0 : nKom(zahl * 10) + 1;
		}

		private int ggt(int a, int b) {
			return (b == 0) ? a : ggt(b, a % b);
		}

	}
}
