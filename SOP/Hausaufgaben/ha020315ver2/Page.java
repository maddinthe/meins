package ha020315ver2;

public class Page {
	private boolean ref;
	private boolean inRam=false;

	public boolean isRef() {
		return ref;
	}

	public void setRef(boolean ref) {
		this.ref = ref;
	}

	public boolean isInRam() {
		return inRam;
	}

	public void setInRam(boolean inRam) {
		this.inRam = inRam;
	}
}
