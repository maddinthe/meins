package ha020315ver2;

public class TestSystem {
	private Liste<Prozess> prozesse = new Liste<>();
	private Liste<Page> pages = new Liste<>();
	int speicher = 1000;
	int auslastung = 0;
	int seitenwechsel=0;

	public TestSystem(int proze) {
		while (proze > 0) {
			prozesse.push(new Prozess(proze));
			proze--;
		}

	}
	public TestSystem(int proze,int ram){
		this(proze);
		speicher=ram;
	}
	public TestSystem() {
		this((int) (Math.random() * 10));
	}
public static void main(String[] args){
	TestSystem t =new TestSystem(20,500);
	System.out.println(t.starten());
}
	
	public int starten(){
		while(!prozesse.isEmpty()){
			for(Prozess p:prozesse){
				Liste<Page> pPages=p.ausfuehren();
				for(Page pa:pPages){
					if (!pa.isInRam()){
						pageLaden(pa);
						p.sWechsel++;
					}
				}
			}	
		}
		return seitenwechsel;		
	}

	public void prozessBeenden(Prozess p){
		System.out.println(p+" wurde beendet");
		prozesse.delete(p);
		
	}
	
 //<<---------------- Seiten Laden -------------- 
	public void pageLaden(Page p){
		if (auslastung==speicher){
			for(Page pa:pages){
				if(pa.isRef()){
					Page po=pages.pop();
					po.setRef(false);
					pages.addEnd(po);
				}else {pages.pop().setInRam(false);;
				pages.addEnd(p);
				p.setInRam(true);}
				break;
			}		
		}else {pages.push(p);
			auslastung++;}
		seitenwechsel++;
		
	}
	
//<<---------------- Subklasse Prozess --------------------	
	public class Prozess {
		private Page[] pages;
		private int counter=1000;
		private int id=0;
		private int sWechsel=0;
		
		
		public Prozess(int nrOfPages,int id){
			this.pages=new Page[nrOfPages];
			for (int i=0;i<pages.length;i++){
				this.pages[i]=new Page();
			}
			this.id=id;
			System.out.println(this+" hat "+this.pages.length+" Pages");
			
		}
		public Prozess(){
			this((int) (Math.random()*100)+1,(int) (Math.random()*100)+1);		
		}
		public Prozess(int id){
			this((int) (Math.random()*100)+1,id);

		}
		public String toString(){
			return "Prozess "+ id+", Seitenwechsel: "+sWechsel;
		}
		
		public Liste<Page> ausfuehren(){
			Liste<Page> ret=new Liste<>();
			for (Page p:pages){
				boolean ref=Math.random()<0.125;
				if (ref==true){
					//counter--;
					p.setRef(ref);
					ret.push(p);
				}
				//if (counter==0) break;
			}
			counter--;
			if (counter==0)
				TestSystem.this.prozessBeenden(this);
			return ret;
		}
		public boolean isFinished(){
			return counter==0;
		}
		
		
		
	}
	
}
