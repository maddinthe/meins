package ha020315ver2;

import java.util.Iterator;


public class Liste<T> implements Iterable<T> {

	private Element<T> first;

	public boolean isEmpty() {
		return first == null;
	}

	public T pop() {
		T ret = first.value;
		first = first.next;

		return ret;
	}

	public void push(T value) {
		Element<T> e = new Element<T>(value);
		e.next = first;
		first = e;
	}

	public void addEnd(T value) {
		Element<T> e = first;
		for (; e.next != null; e = e.next)
			;
		e.next = new Element<T>(value);
	}

	public void delete(T value){
		Element<T> ele, last = first;

		// Liste leer
		if (first == null)
			return;

		// Erstes Element matcht
		if (first.value.equals(value)) {
			first = first.next;
			return;
		}

		// die weiteren Elemente werden untersucht
		ele = last.next;

		while (ele != null) {
			if (ele.value.equals(value)) {
				last.next = (ele.next);
				break; // hier abbrechen, da Element gefunden wurde
			}

			last = ele;
			ele = ele.next;
		}
	}
	public Iterator<T> iterator() {
		return new Aufzaehler();
	}

	private class Aufzaehler implements Iterator<T> {
		private Element<T> current = first;

		public boolean hasNext() {
			return current != null;
		}

		public T next() {
			T ret = current.value;
			current = current.next;
			return ret;
		}

		public void remove() {
		}

	}

	private static class Element<T> {
		private T value;
		private Element<T> next;

		public Element(T value) {
			this.value = value;
		}
	}

}
