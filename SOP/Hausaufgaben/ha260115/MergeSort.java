package ha260115;

public class MergeSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//char[] bla = { 'a', 'b', 'e', 'h', 'k', 'e', 'a', 'g', 'i', 'e', 'v',
			//	'h', 'e', 'k', 'p', 'a', 'w', 't' };

		char[] test = new char[1000000];
		for (int i = 0; i < test.length; i++)
			test[i] = (char) ((int) (Math.random() * 26) + 97);
		for (char a : test)
			System.out.println(a);
		long start = System.currentTimeMillis();
		test = mergeSort(test);
		System.out.println(System.currentTimeMillis() - start + " MS");
		for (char a : test)
			System.out.println(a);

		// System.out.println(bla);
		// System.out.println(mergeSort(bla));
	}

	public static char[] mergeSort(char[] arr) {

		// -----Abbbruchbedingung
		if (arr.length < 2)
			return arr;

		// ----- Arr A erschaffen und mit der ersten H�lfte f�llen
		char[] a = new char[arr.length / 2];
		for (int i = 0; i < a.length; i++)
			a[i] = arr[i];

		// arr B erschaffen und mit der zweiten H�lfte f�llen
		char[] b = new char[arr.length - a.length];
		for (int i = 0; i < b.length; i++)
			b[i] = arr[i + a.length];

		// ---- A und B sortieren lassen
		a = mergeSort(a);
		b = mergeSort(b);

		// ----- A und B in das neue Array einsortieren bis einer von beiden
		// fertig einsortiert ist
		char[] newArr = new char[arr.length];
		int i = 0, j = 0, k = 0;
		while (j < a.length && k < b.length)
			if (a[j] < b[k])
				newArr[i++] = a[j++];
			else
				newArr[i++] = b[k++];

		// ------den rest von A einsortieren falls vorhanden
		while (j < a.length)
			newArr[i++] = a[j++];

		// ------ den rest von B einsotieren falls vorhanden
		while (k < b.length)
			newArr[i++] = b[k++];

		// ----- return des neuen Arrays
		return newArr;
	}
}
