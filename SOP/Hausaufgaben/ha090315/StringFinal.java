package ha090315;
import java.util.Scanner;  //Scanner importieren
public class StringFinal {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Gib einen Satz ein:");	
		Scanner s = new Scanner(System.in);						// Setze einen Scanner im Programm
		String a = s.nextLine();								// speichere deine Eingabe im String a
		String b = new String();								// lege einen neuen String b an
		String c = new String();								// lege einen neuen String c an
		int[] gross = new int[a.length()];						// lege ein integer array gross an mit der länge vom string a
		char[] hilfe = new char[a.length()];					// lege ein character array hilfe an mit der länge vom string a
		int temp = 0;											// lege eine integer variable temp an die am anfang null ist
		for (int i = 0; i <= a.length() - 1; i++) {				// durchlaufe den string a bis zum ende
			if (Character.isLetter(a.charAt(i))== false) {		// überprüfe ob an einer Stelle kein Buchstabe vorliegt
				for (int j = i - 1; j >= temp; j--) {			// durchlaufe den String ab der gefundenen bedingung rückwärts bis zu temp 
					b += a.charAt(j);							// schreibe meine zeichen von a in meinen string b
				}
				temp = b.length() + 1;							// speichere in temp die länge von b+1
				b += a.charAt(i);								// schreibe meine Buchstaben von a in meinen string b
			} else if (i == a.length() - 1) {					// wenn im durchlauf die länge des strings a erreicht ist
				for (int j = i; j >= temp; j--) {				// durchlaufe den string a rückwärts bis zu temp
					b += a.charAt(j);							// schreibe meine Buchstaben in meinen string b		
				}
			}			
		}
		b = b.toLowerCase();									// schreibe jeden Buchstaben in meinem string b klein
		for (int i = 0; i <= a.length() - 1; i++) {				// durchlaufe den string a bis zum ende
			if (Character.isUpperCase(a.charAt(i)) == true) {	// überprüfe ob ein großbuchstabe vorhanden ist
				gross[i] = 1;									// schreibe an der Stelle i eine 1 ins array
			}
			hilfe[i] = b.charAt(i);								// schreibe in meine hilfe array den kompletten string b
			if (gross[i] == 1) {								// überprüfe an welcher stelle im gross array eine 1 steht
				hilfe[i] = Character.toUpperCase(hilfe[i]);		// schreibe den Buchstaben an der Stelle i gross
			}
			c += hilfe[i];										// schreibe im string c das hilfe array
		}
		System.out.println(c);									// gib den String c aus
	}
}
