package ha090315;

public class Liste {
	Element first;
	
	public boolean isEmpty(){
		return first==null;
	}
	
	public void add(Object value){
		if(isEmpty()) first=new Element(value);
		else {
			Element ele=new Element(value);
			ele.next=first;
			first=ele;
		}
	}
	
	public Object pop(){
		if (isEmpty()) return null;
		Object value=first.value;
		first=first.next;
		return value;
		
	}
	
	public void addEnde(Object value){
		Element ele=first;
		while(ele.next!=null)
			ele=ele.next;
		ele.next=new Element(value);
		
		
	}
	
	public String toString(){
		String ret="";
		for(Element ele=first;ele!=null;ele=ele.next)
			ret+=ele.value;
		return ret;
	}
	
	
	private class Element{
		private Element next;
		private Object value;
		
		public Element(Object value){
			this.value=value;
		}
	}
}
