package ha110515;

public class Ausfuehrer {

	public static void main(String[] args) {
		Simpson[] mitarbeiter={new Simpson("Homer","Simpson"),new Simpson("Lenny","Leonard"),new Simpson("Carl","Carlson"),new Simpson("Mindy","Simmons")};
		Simpson[] kinder={new Simpson("Bart","Simpson"),new Simpson("Lisa","Simpson")};
		Kalkulator k=new Kalkulator();
		k.addMitarbeiter(mitarbeiter[0], false, true);
		k.addMitarbeiter(mitarbeiter[1], false, true);
		k.addMitarbeiter(mitarbeiter[2], false, true);
		k.addMitarbeiter(mitarbeiter[3], true, false);
		k.addKind(mitarbeiter[0], kinder[0], null);
		k.addKind(mitarbeiter[0], kinder[1], null);
		
		System.out.println(k.anzGaeste());
		System.out.println(k.anzStammGast());
		k.printEinladungen();
		
		k.printEinladungenAlpha();
		
	}

}
