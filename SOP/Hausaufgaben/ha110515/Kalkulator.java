package ha110515;

import java.util.*;

public class Kalkulator {

	private Map<Simpson, Simpson> vaeter = new HashMap<>();
	private Map<Simpson, Simpson> muetter = new HashMap<>();
	private Set<Simpson> mitarbeiter = new HashSet<>();
	private Set<Simpson> weiblich = new HashSet<>();
	private Set<Simpson> stammGast = new HashSet<>();

	
	public void addMitarbeiter(Simpson mitarb,boolean isWeiblich, boolean isStammGast){
		mitarbeiter.add(mitarb);
		if (isWeiblich) weiblich.add(mitarb);
		if (isStammGast) stammGast.add(mitarb);		
	}
	public void addKind(Simpson kind, Simpson vater, Simpson mutter){
		if (vater!=null) vaeter.put(kind, vater);
		if (mutter!=null)muetter.put(kind, mutter);
	}
	
	public int anzStammGast() {
		return stammGast.size();
	}

	public int anzGaeste() {
		int ret = mitarbeiter.size() + vaeter.size();
		Collection<Simpson> temp = muetter.keySet();
		temp.removeAll(vaeter.keySet());
		return ret + temp.size();
	}

	public void printEinladungen() {
		for (Simpson s : mitarbeiter) {
			String print;
			boolean isWeiblich = weiblich.contains(s);
			if (isWeiblich)
				print = "Werte Mrs." + s.getVorname() + " " + s.getNachname()
						+ ",hiermit lade ich Sie ";
			else
				print = "Werter Mr." + s.getVorname() + " " + s.getNachname()
						+ ",hiermit lade ich Sie ";
			int kinder = 0;
			if (!isWeiblich)
				for (Simpson t : vaeter.values()) {
					if (t.equals(s))
						kinder++;
					if (kinder > 1)
						break;
				}
			else
				for (Simpson t : muetter.values()) {
					if (t.equals(s))
						kinder++;
					if (kinder > 1)
						break;
				}

			if (kinder > 0)
				if (kinder > 1)
					print += " und ihre Kinder";
				else
					print += " und ihr Kind";

			System.out.println(print + "herzlich zum Betriebsfest ein.");

		}
	}

	public void printEinladungenAlpha() {
		Set<Simpson> maenner = mitarbeiter;
		maenner.removeAll(weiblich);

		for (Simpson s : maenner) {
			String print = "Werter Mr." + s.getVorname() + " "
					+ s.getNachname() + ",hiermit lade ich Sie ";
			int kinder = 0;
			for (Simpson t : vaeter.values()) {
				if (t.equals(s))
					kinder++;
				if (kinder > 1)
					break;
			}

			if (kinder > 0)
				if (kinder > 1)
					print += " und ihre Kinder";
				else
					print += " und ihr Kind";

			System.out.println(print + "herzlich zum Betriebsfest ein.");

		}

		for (Simpson s : weiblich) {
			String print = "Werte Mrs." + s.getVorname() + " "
					+ s.getNachname() + ",hiermit lade ich Sie ";
			int kinder = 0;
			for (Simpson t : muetter.values()) {
				if (t.equals(s))
					kinder++;
				if (kinder > 1)
					break;
			}

			if (kinder > 0)
				if (kinder > 1)
					print += " und ihre Kinder";
				else
					print += " und ihr Kind";

			System.out.println(print + "herzlich zum Betriebsfest ein.");
		}
	}
}
