package ha110515;

public class Simpson implements Comparable<Simpson> {

	private String nachname;
	private String vorname;

	public Simpson(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
	}

	public String getNachname() {
		return nachname;
	}

	public String getVorname() {
		return vorname;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((nachname == null) ? 0 : nachname.hashCode());
		result = prime * result + ((vorname == null) ? 0 : vorname.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Simpson other = (Simpson) obj;
		if (nachname == null) {
			if (other.nachname != null)
				return false;
		} else if (!nachname.equals(other.nachname))
			return false;
		if (vorname == null) {
			if (other.vorname != null)
				return false;
		} else if (!vorname.equals(other.vorname))
			return false;
		return true;
	}

	public int compareTo(Simpson o) {
		int nachn=nachname.compareTo(o.nachname);
		return (nachn==0)?vorname.compareTo(o.vorname):nachn;
	}

}
