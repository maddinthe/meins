package januar15;

public class IntElement {
	
		private int value;
		private IntElement next=null;

		public IntElement(int value) {
			this.value = value;
		}

		public void setNext(IntElement next) {
			this.next = next;
		}

		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}

		public IntElement getNext() {
			return next;
		}
}
