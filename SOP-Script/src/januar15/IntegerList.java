package januar15;

public class IntegerList {
	private IntElement first = null;

	public boolean isEmpty() {
		return first == null;
	}

	public void add(int value) {
		if (first != null) {
			IntElement next = first;
			first = new IntElement(value);
			first.setNext(next);
		} else
			first = new IntElement(value);
	}
	public int pop() {
		int tmp = first.getValue();
		first = first.getNext();
		return tmp;
	}

	public String toString() {
		String ausg = "[";
		IntElement next = first;
		while (true) {
			if (next == null) {
				if (ausg != "[")
					ausg = ausg.substring(0, ausg.length() - 2) + "]";
				else
					ausg += "]";
				break;
			}
			ausg += next.getValue() + ", ";
			next = next.getNext();
		}

		return ausg;
	}

	public void sort() {
		if (first == null || first.getNext() == null)
			return;

		IntegerList tempL = new IntegerList();

		tempL.add(first.getValue());
		first = first.getNext();

		IntElement ele = first;
		while (ele != null) {
			IntElement newEle = new IntElement(ele.getValue());

			if (newEle.getValue() > tempL.first.getValue()) {
				newEle.setNext(tempL.first);
				tempL.first = newEle;
			} else {
				IntElement innerEle = tempL.first;
				boolean isLast = true;

				while (innerEle != null) {
					if (newEle.getValue() > innerEle.getNext().getValue()) {
						newEle.setNext(innerEle.getNext());
						innerEle.setNext(newEle);
						isLast = false;
						break;
					}
					innerEle = innerEle.getNext();
				}
				if (isLast)
					innerEle.setNext(newEle);
			}

			ele = ele.getNext();
		}

		first = tempL.first;
	}

	public void sortiere(){
		IntegerList list=new IntegerList();
		list.first=first;
		first=new IntElement(list.pop());
		while(!list.isEmpty()){
			insertSort(list.pop());
		}
		
	}
	public IntegerList copy(){
		IntegerList temp=new IntegerList();
		if(isEmpty())return temp;
		temp.first=new IntElement(first.getValue());
		for (IntElement ele=first,nele=temp.first;ele.getNext()!=null;ele=ele.getNext(),nele=nele.getNext()){
			nele.setNext(new IntElement(ele.getNext().getValue()));
		}
		
		return temp;
	}
	
	public IntegerList reCopy(){
		IntegerList ret=new IntegerList();
		ret.first=reCopy(first);
		return ret;
	}
	
	private IntElement reCopy(IntElement ele){
		IntElement ne=new IntElement(ele.getValue());
		if(ele.getNext()!=null)ne.setNext(reCopy(ele.getNext()));
		return ne;
	}
	
	public void insertSort(int neu) {
		if (first == null)
			first = new IntElement(neu);
		else if (neu>first.getValue()){
			IntElement ne=new IntElement(neu);
			ne.setNext(first);
			first=ne;
		}
		else {
			for (IntElement temp = first;; temp = temp.getNext()) {
				if (temp.getNext() == null) {
					temp.setNext(new IntElement(neu));
					return;
				}
				if (neu >= temp.getNext().getValue()) {
					IntElement ne = new IntElement(neu);
					ne.setNext(temp.getNext());
					temp.setNext(ne);
					return;
				}
			}
		}
	}

}
