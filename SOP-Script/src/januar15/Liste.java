package januar15;

public class Liste {
	private Element first;

	public boolean isEmpty() {
		return first == null;
	}

	public void addEnd(String neu) {
		insert(0, neu);
	}

	public void add(String value) {
		if (first != null) {
			Element next = first;
			first = new Element(value);
			first.setNext(next);
		} else
			first = new Element(value);
	}

	public String toString() {
		String ausg = "[";
		Element next = first;
		while (true) {
			if (next == null) {
				if (ausg != "[")
					ausg = ausg.substring(0, ausg.length() - 2) + "]";
				else
					ausg += "]";
				break;
			}
			ausg += next.getValue() + ", ";
			next = next.getNext();
		}

		return ausg;
	}

	public int getSize() {
		int groesse = 0;
		Element next = first;
		for (; next != null; groesse++) {
			next = next.getNext();
		}
		return groesse;
	}

	public void delete(String value) {
		if (isEmpty())
			return;
		if (first.getValue().contentEquals(value)) {
			first = first.getNext();
			return;
		}
		Element current = first;
		Element old = first;
		while (true) {
			if (current.getValue().contentEquals(value)) {
				old.setNext(current.getNext());
				return;
			} else if (current.getNext() == null) {
				return;
			} else {
				old = current;
				current = current.getNext();
			}

		}
	}

	public void push(String arg0) {
		this.add(arg0);
	}

	public String pop() {
		if (isEmpty())
			return null;
		String tmp = first.getValue();
		first = first.getNext();
		return tmp;
	}

	public int getIndex(String value) {
		if (isEmpty())
			return -1;
		int size = this.getSize();
		if (first.getValue().equals(value))
			return size - 1;
		Element current = first;
		while (true) {
			if (current.getValue().contentEquals(value)) {
				return size - 1;
			} else if (current.getNext() == null) {
				return -1;
			} else {
				current = current.getNext();
				size--;
			}
		}
	}

	public String atIndex(int i) {
		if (isEmpty())
			return null;
		int size = this.getSize();
		if (i == size - 1)
			return first.getValue();
		else {
			Element current = first;
			for (int j = 0; j < size - i - 1; j++) {
				current = current.getNext();
			}
			return current.getValue();
		}

	}

	public String min() {
		if (isEmpty())
			return null;
		String kleinst = first.getValue();
		Element e = first;
		for (; e.getNext() != null; e = e.getNext()) {
			if (kleinst.compareTo(e.getValue()) > 0) {
				kleinst = e.getValue();
			}
		}

		return kleinst;
	}

	public String minR() {
		if (first == null)
			return null;
		return first.min();

	}

	public String remove(int index) {
		if (isEmpty())
			return null;
		int size = getSize() - 1;
		if (size - index < 0)
			return null;
		if (index == size) {
			String temp = first.getValue();
			first = first.getNext();
			return temp;
		}
		return remove(first, size - 1 - index);
	}

	private String remove(Element ele, int zahl) {
		if (zahl == 0) {
			String temp = ele.getNext().getValue();
			ele.setNext(ele.getNext().getNext());
			return temp;
		}
		return remove(ele.getNext(), zahl - 1);
	}

	public void insert(int index, String arg) {
		int size = getSize();
		if (index > size - 1)
			return;

		Element tmp = new Element(arg);
		// if (index == size - 1) {
		// tmp.setNext(first);
		// first = tmp;
		// }
		Element tmp2 = first;
		for (int i = 1; i < size - index; i++) {
			tmp2 = tmp2.getNext();
		}
		tmp.setNext(tmp2.getNext());
		tmp2.setNext(tmp);
	}

	public void minSort() {
		Liste neu = new Liste();
		while (!isEmpty()) {
			String temp = minR();
			neu.add(temp);
			delete(temp);
		}
		first = neu.first;
	}

	public void minSort2() {
		Liste neu = new Liste();
		Element current = first;
		Element old = first;
		Element kleinst = first;
		while (!isEmpty()) {
			for (; current.getNext() != null; current = current.getNext()) {
				if (current.getNext().getValue().compareTo(kleinst.getValue()) < 0) {
					old = current;
					kleinst = current.getNext();
				}
			}
			old.setNext(kleinst.getNext());
			kleinst.setNext(neu.first);
			neu.first=kleinst;
			current=first;
			

		}
		first=neu.first;
		

	}

}
