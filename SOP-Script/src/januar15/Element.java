package januar15;

public class Element {
	private String value;
	private Element next=null;

	public Element(String value) {
		super();
		this.value = value;
	}

	public void setNext(Element next) {
		this.next = next;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Element getNext() {
		return next;
	}
	public String min(){
		if (next==null)
			return value;
		String min=next.min();
		if (value.compareTo(min)<0)
		 return value;
		return min;
	}

}
