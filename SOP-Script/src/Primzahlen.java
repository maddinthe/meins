public class Primzahlen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] zahlen = new int[1000];
		for (int i = 0; i < zahlen.length; i++)
			zahlen[i] = i + 2;

		for (int i = 0; i < zahlen.length; i++)
			if (zahlen[i] != 0)
				for (int k = zahlen[i] + i; k < zahlen.length; k += zahlen[i])
					zahlen[k] = 0;

		int l = 0;
		for (int a : zahlen)
			if (a != 0)
				l++;
		int[] prim = new int[l];

		for (int i = 0, a = 0; i < zahlen.length; i++)
			if (zahlen[i] != 0)
				prim[a++] = zahlen[i];

		for (int i : prim)
			System.out.println(i);

	}
}
