import java.util.Scanner;

public class Crypto {

	public static void main(String[] args) {
		Crypto c = new Crypto();
		Scanner s=new Scanner(System.in);
		String geheim=s.nextLine();
		String versch=c.encrypt(geheim);
		String entsch=c.decrypt(versch);
		System.out.println(geheim);
		System.out.println(versch);
		System.out.println(entsch);

	}

	private char[] key;

	private void generateKey(int length) {

		char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		key=new char[length];
		for (int i = 0; i < length; i++)
			key[i] = digits[(int)(Math.random()*digits.length)];
		
		
	}

	public String encrypt(String message) {

		generateKey(message.length());
		int[] geheimeMessage = new int[message.length()];
		String geheimTextString = "";

		for (int i = 0; i < message.length(); i++) {
			geheimeMessage[i] = key[i] ^ message.charAt(i);
			geheimTextString += (char) geheimeMessage[i];
		}

		return geheimTextString;

	}

	public String decrypt(String geheimeMessage) {
		String entsch="";
		int[] entschArr=new int[geheimeMessage.length()];
		
		for (int i=0; i < geheimeMessage.length();i++){
			entschArr[i]= key[i]^geheimeMessage.charAt(i);
			entsch += (char)entschArr[i];
		}
		return entsch;

	}
}
