package weihnachten;

public class Tester {

	public static void main(String[] args) {
		Weihnachtsbaum[] wb = new Weihnachtsbaum[20];

		for (int i = 0; i < wb.length; i++)
			wb[i] = new Weihnachtsbaum();

		double min = wb[0].getGroesse();
		for (Weihnachtsbaum w : wb) {
			if (w != null && w.getGroesse() < min)
				min = w.getGroesse();
		}
		Weihnachtsbaum[] sort = new Weihnachtsbaum[wb.length];
		for (int i = 0, k = 0; i < wb.length; i++) {
			min = Double.MAX_VALUE;
			for (int j = 0; j < wb.length; j++) {
				if (wb[j] != null && wb[j].getGroesse() < min) {
					k = j;
					min = wb[k].getGroesse();
				}
			}
			sort[i] = wb[k];
			wb[k] = null;
			k = 0;
		}

		System.out.println("der kleinste Baum ist " + sort[0].getGroesse()
				+ "m gro�!");

		// for (Weihnachtsbaum w:wb)
		// System.out.println(w.getGroesse()+"m");

		for (Weihnachtsbaum w : sort)
			System.out.println(w.getGroesse() + "m");

		Weihnachtsbaum[] wbsort = new Weihnachtsbaum[20];
		for (int i = 0; i < wbsort.length; i++)
			wbsort[i] = new Weihnachtsbaum();

		Weihnachtsbaum temp;
		for (int j = 0; j < wbsort.length; j++) {
			temp = wbsort[j];
			int k = j;
			while (k > 0 && wbsort[k - 1].getGroesse() > temp.getGroesse()) {
				wbsort[k] = wbsort[k - 1];
				k--;
			}
			wbsort[k] = temp;
		}

		for (Weihnachtsbaum w : wbsort)
			System.out.println(w.getGroesse() + " m");
	}
}
