package april_15;

public class BinaryTree<T extends Comparable<T>> {
	private Node root;
	
	public BinaryTree(){
		
	}
	public BinaryTree(T value){
		root=new Node(value);
	}
	public BinaryTree(BinaryTree<T> left,T value,BinaryTree<T> right){
		Node leftNode= left!=null?left.root:null;
		Node rightNode=right!=null?right.root:null;
		root=new Node(leftNode, value,rightNode);
	}
	
	public boolean isEmpty(){
		return root==null;
	}
	public BinaryTree<T> getLeft(){
		BinaryTree<T> ret=new BinaryTree<T>();
		if (root.left==null) return ret;
		ret.root=root.left;
		return ret;
		
	}
	
	public BinaryTree<T> getRight(){
		BinaryTree<T> ret=new BinaryTree<T>();
		if (root.right==null) return ret;		
		ret.root=root.right;
		return ret;
	}
	
	public T getRootData(){
		return root.value;
	}
	
	public String toString(){
		if(isEmpty())return "";
		return getLeft().toString()+" "+root.value+" "+getRight().toString();
	}
	
	public int getSize(){
		if(isEmpty())return 0;
		return getLeft().getSize()+1+getRight().getSize();
		
	}
	public boolean contains(T value){
		if (isEmpty())return false;
		if (value.equals(getRootData())) return true;
		if (value.compareTo(getRootData())<0)
			return getLeft().contains(value);
		else return getRight().contains(value);
		
	}
	
	public int getDeep(){
		if (isEmpty())return 0;
		int leftDeep=1+getLeft().getDeep();
		int rightDeep=1+getRight().getDeep();
		return leftDeep<rightDeep?rightDeep:leftDeep;
		
	}
	
	
	
	private class Node{
		private Node left;
		private Node right;
		private T value;
		
		public Node(Node left,T value, Node right){
			this.left=left;
			this.right=right;
			this.value=value;
		}
		public Node(T value){
			this.value=value;
		}
		
	}
}
