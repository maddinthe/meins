package april_15;

public class Test {

	public static void main(String[] args) {
		BinaryTree<Integer> a=new BinaryTree<>(1);
		BinaryTree<Integer> b=new BinaryTree<>(3);
		BinaryTree<Integer> c=new BinaryTree<>(a,2,b);
		
		BinaryTree<Integer> d=new BinaryTree<>(5);
		BinaryTree<Integer> e=new BinaryTree<>(7);
		BinaryTree<Integer> f=new BinaryTree<>(d,6,e);
		
		BinaryTree<Integer> g=new BinaryTree<>(c,4,f);
		System.out.println(c);
		System.out.println(g);
		
		System.out.println(g.getSize());
		
		System.out.println(g.contains(7));
		System.out.println(g.getDeep());
	}

}
