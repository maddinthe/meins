package februar_15;

public class Element {
	private Element next;
	private Element prev;
	private Object value;

	public Element(Object value) {
		this.value = value;
	}

	public Element getNext() {
		return next;
	}

	public void setNext(Element next) {
		this.next = next;
	}

	public Element getPrev() {
		return prev;
	}

	public void setPrev(Element prev) {
		this.prev = prev;
	}

	public Object getValue() {
		return value;
	}
}
