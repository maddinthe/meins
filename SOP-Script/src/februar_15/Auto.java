package februar_15;

public class Auto {
	private String hersteller;
	private String modell;

	public Auto(String hersteller, String modell) {
		this.hersteller = hersteller;
		this.modell = modell;
	}


	public String toString() {
		return hersteller +", "+ modell ;
	}

	public String getHersteller() {
		return hersteller;
	}

	public String getModell() {
		return modell;
	}
}
