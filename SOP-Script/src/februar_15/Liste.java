package februar_15;

public class Liste {
	Element first = null;
	Element last = null;

	public boolean isEmpty() {
		return first == null;
	}

	public void add(Object o) {
		Element e = new Element(o);
		if (isEmpty()) {
			first = e;
			last = e;
		} else {
			last.setNext(e);
			e.setPrev(last);
			last = e;
		}
	}
	
	public String toString(){
		if(isEmpty()) return "[]";
		String s="[";
		for(Element e=first;e!=null;e=e.getNext())
			s+=e.getValue().toString()+"; ";
		return s.substring(0,s.length()-2)+"]";
	}
	
	public String toStringReverse(){
		if(isEmpty()) return "[]";
		String s="[";
		for(Element e=last;e!=null;e=e.getPrev())
			s+=e.getValue().toString()+"; ";
		return s.substring(0,s.length()-2)+"]";
	}
	
	public void remove(Object o){
		if (isEmpty())return;
		if (first==last){
			if (first.getValue()==o){
				first=null;
				last=null;
				return;
			}
			else return;
		}
		if (first.getValue()==o){
			first=first.getNext();
			first.setPrev(null);
		}
		if (last.getValue()==o){
			last=last.getPrev();
			last.setNext(null);
		}
			
		for (Element e=first;e!=last;e=e.getNext())
			if (e.getValue()==o){
				e.getPrev().setNext(e.getNext());
				e.getNext().setPrev(e.getPrev());
				return;
			}		
		return;
	}
	public void insertAfter(Object neu, Object after){
		Element ele=first;
		for (;ele.getValue()!=after;ele=ele.getNext()){
			if (ele.getNext()==null)break;
		}
		Element neuEle=new Element(neu);
		neuEle.setNext(ele.getNext());
		neuEle.setPrev(ele);
		if (ele==last)last=neuEle;
		else ele.getNext().setPrev(neuEle);
		ele.setNext(neuEle);
		
	}
	

}
