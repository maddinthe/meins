package mai_15;

public class TreeMap<K extends Comparable<K>, V> {

	private Node root;

	public boolean isEmpty() {
		return root == null;
	}

	public TreeMap<K, V> getLeft() {
		TreeMap<K, V> left = new TreeMap<>();
		left.root = root.left;
		return left;
	}

	public TreeMap<K, V> getRight() {
		TreeMap<K, V> right = new TreeMap<>();
		right.root = root.right;
		return right;
	}

	public V put(K key, V value) {
		if (isEmpty())
			root = new Node(null, key, value, null);
		else if (key.compareTo(root.key) < 0) {
			if (root.left == null)
				root.left = new Node(null, key, value, null);
			else
				getLeft().put(key, value);
		} else if (key.compareTo(root.key) > 0)
			if (root.right == null)
				root.right = new Node(null, key, value, null);
			else
				getRight().put(key, value);
		else root.value=value;
		return value;
	}
	
	public V get(K key){
		if (isEmpty())
			return null;
		if (key.compareTo(root.key) < 0)
			return getLeft().get(key);
		if (key.compareTo(root.key) > 0)
			return getRight().get(key);
		return root.value;
	}
	
	
	

	private class Node {
		private Node left;
		private K key;
		private V value;
		private Node right;

		public Node(Node left, K key, V value, Node right) {
			super();
			this.left = left;
			this.key = key;
			this.value = value;
			this.right = right;
		}

	}

}
