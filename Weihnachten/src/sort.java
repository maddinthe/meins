

public class sort {

	public void bubbsortiere(int[] arr){
		boolean tausch=true;
		for(int j=1;tausch;j++){
			tausch=false;
			for (int i=0;i<arr.length-j;i++){
				if (arr[i]>arr[i+1]){
					int temp=arr[i];
					arr[i]=arr[i+1];
					arr[i+1]=temp;
					tausch=true;
				}
			}
		}
	}
	public void autosortKennz(Auto[] autos){
		boolean tausch=true;
		for(int j=1;tausch;j++){
			tausch=false;
			for (int i=0;i<autos.length-j;i++){
				Auto temp=null;
				if (autos[i].getKennzeichen().compareToIgnoreCase(autos[i+1].getKennzeichen())>0){
					temp=autos[i];
					autos[i]=autos[i+1];
					autos[i+1]=temp;
					tausch=true;
				}
			}
		}
	}
	
	
	public boolean lexikografisch(String t1,String t2){
		String s1=t1.toUpperCase();
		String s2=t2.toUpperCase();
		for(int i=0;i<s1.length()&&i<s2.length();i++)
		if (s1.charAt(i)>s2.charAt(i))
			return true;
		else if (s1.charAt(i)<s2.charAt(i))
			return false;
		return s1.length()>s2.length();
	}
	
}
