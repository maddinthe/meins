
public class CharStack {
	private char[] charArr = new char[0];

	public boolean isEmpty() {
		return charArr.length == 0;
	}

	public void push(char character) {
		char[] newArr = new char[charArr.length + 1];
		for (int i = 0; i < charArr.length; i++)
			newArr[i + 1] = charArr[i];
		newArr[0] = character;
		charArr = newArr;
	}
	public char pop(){
		char ret=charArr[0];
		char[] newArr=new char[charArr.length-1];
		for (int i=0;i<newArr.length;i++)
			newArr[i]=charArr[i+1];
		charArr=newArr;
		return ret;
	}
}

