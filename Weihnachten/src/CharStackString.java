public class CharStackString {
	private String charArr = "";

	public boolean isEmpty() {
		return charArr.length() == 0;
	}

	public void push(char character) {
		charArr = character + charArr;
	}

	public char pop() {
		char ret = charArr.charAt(0);
		if (charArr.length() == 1)
			charArr = "";
		else charArr = charArr.substring(1);
		return ret;
	}
}
