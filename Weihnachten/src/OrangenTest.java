public class OrangenTest {

	public static void main(String[] args) {
		String[][] daten = { { "true", "Navel" },
				{ "true", "Navel", "false", "0.5" },
				{ "false", "Blut", "0.4" }, { "true", "Blut", "true" }, };
		Orange[] orangen = new Orange[daten.length];
		for (int i = 0; i < daten.length; i++) {
			if (daten[i].length == 2)
				orangen[i] = new Orange(Boolean.parseBoolean(daten[i][0]),
						daten[i][1]);
			else if ((daten[i][2] == "true" || daten[i][2] == "false")
					&& daten[i].length == 3)
				orangen[i] = new Orange(Boolean.parseBoolean(daten[i][0]),
						daten[i][1], Boolean.parseBoolean(daten[i][2]));
			else if ((daten[i][2] == "true" || daten[i][2] == "false")
					&& daten[i].length == 4)
				orangen[i] = new Orange(Boolean.parseBoolean(daten[i][0]),
						daten[i][1], Boolean.parseBoolean(daten[i][2]),
						Double.parseDouble(daten[i][3]));
			else
				orangen[i] = new Orange(Boolean.parseBoolean(daten[i][0]),
						daten[i][1], Double.parseDouble(daten[i][2]));
		}
		for (int i = 1, tausch = 1; tausch == 1; i++) {
			tausch = 0;
			for (int j = 0; j < orangen.length - i; j++) {
				if (orangen[j].getGewicht() > orangen[j + 1].getGewicht()) {
					Orange tmp = orangen[j];
					orangen[j] = orangen[j + 1];
					orangen[j + 1] = tmp;
					tausch = 1;
				}
			}
		}
		for (Orange o : orangen)
			System.out.println(o);
	}
}
