package sortiern;

public class SortTest {

	public static void main(String[] args) {
		int[] arr={5,8,2,4,3245,1,3,89,3,678,12};
			
		boolean tausch=true;
		for(int i=1;tausch;i++){
			tausch=false;
			for (int j=0;j<arr.length-i;j++){
				if(arr[j]>arr[j+1]){
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
					tausch=true;
				}
			}
		}
		for(int i:arr)
			System.out.println(i);
	}

}
