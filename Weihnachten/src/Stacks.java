
public class Stacks {

	public static void main(String[] args) {

		IntStack myStack = new IntStack();
		myStack.push(1);
		myStack.push(2);
		myStack.push(3);
		while (!myStack.isEmpty())
			System.out.println(myStack.pop());

		long start1 = System.currentTimeMillis();
		CharStack charS = new CharStack();
		for (int i = 0; i < 10000; i++)
			charS.push('a');

		while (!charS.isEmpty())
			charS.pop();
		long end1 = System.currentTimeMillis();
		System.out.println("Laufzeit: " + (end1 - start1) + " ms");

		long start2 = System.currentTimeMillis();
		CharStackString charStr = new CharStackString();
		for (int i = 0; i < 10000; i++)
			charStr.push('a');

		while (!charStr.isEmpty())
			charStr.pop();
		long end2 = System.currentTimeMillis();
		System.out.println("Laufzeit: " + (end2 - start2) + " ms");

		ObjStack obj = new ObjStack();
		obj.push(new Object());
		obj.push(new Object());
		obj.push(new Object());
		while (!obj.isEmpty())
			System.out.println(obj.pop());
	}
}
