public class Auto {
	private String farbe;
	private String kennzeichen;
	private double hubraum;
	private int sitzplaetze;
	private String fahrgestellnummer;

	public Auto(String fahrgestellnummer) {
		this.fahrgestellnummer = "zufall+"+fahrgestellnummer;
	}

	public Auto(String farbe, String kennzeichen, double hubraum,
			int sitzplaetze, String fahrgestellnummer) {
		this(fahrgestellnummer);
		this.farbe = farbe;
		this.kennzeichen = kennzeichen;
		this.hubraum = hubraum;
		this.sitzplaetze = sitzplaetze;

	}
	public String toString(){
		return kennzeichen;
	}

	public String getFahrgestellnummer() {
		return fahrgestellnummer;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public String getKennzeichen() {
		return kennzeichen;
	}

	public double getHubraum() {
		return hubraum;
	}

	public void setHubraum(double hubraum) {
		this.hubraum = hubraum;
	}

	public int getSitzplaetze() {
		return sitzplaetze;
	}

	public void setSitzplaetze(int sitzplaetze) {
		this.sitzplaetze = sitzplaetze;
	}

}
