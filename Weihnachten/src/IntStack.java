public class IntStack {
	private int[] intArr = new int[0];

	public boolean isEmpty() {
		return intArr.length == 0;
	}

	public void push(int in) {
		int[] newArr = new int[intArr.length + 1];
		for (int i = 0; i < intArr.length; i++)
			newArr[i + 1] = intArr[i];
		newArr[0] = in;
		intArr = newArr;
	}
	public int pop(){
		int ret=intArr[0];
		int[] newArr=new int[intArr.length-1];
		for (int i=0;i<newArr.length;i++)
			newArr[i]=intArr[i+1];
		intArr=newArr;
		return ret;
	}
}
