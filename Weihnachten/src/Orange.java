
public class Orange {
	private boolean reif;
	private double gewicht;
	private boolean kerne;
	private String sorte;
	
	
	public Orange(boolean kerne, String sorte) {
		super();
		this.kerne = kerne;
		this.sorte = sorte;
	}
	public Orange(boolean kerne,String sorte,boolean reif){
		this(kerne,sorte);
		this.reif=reif;
	}
	public Orange(boolean kerne,String sorte,double gewicht){
		this(kerne,sorte);
		this.gewicht=gewicht;
	}
	public Orange(boolean kerne,String sorte,boolean reif,double gewicht){
		this(kerne,sorte,reif);
		this.gewicht=gewicht;
	}
	
	@Override
	public String toString() {
		return "Orange [reif=" + reif + ", gewicht=" + gewicht + ", kerne="
				+ kerne + ", sorte=" + sorte + "]";
	}
	public boolean isReif() {
		return reif;
	}
	public double getGewicht() {
		return gewicht;
	}
	public boolean isKerne() {
		return kerne;
	}
	public String getSorte() {
		return sorte;
	}
}
