package euler21to40;

import java.util.*;

public class Euler35CircularPrimes {

	public static void main(String[] args) {
		Set<Integer> primzahlen = sieb(999999);
		System.out.println(primzahlen.size());
		Map<Integer, Set<Integer>> getrennt = new HashMap<>();
		for (int i = 1; i < 7; i++)
			getrennt.put(i, new HashSet<Integer>());
		for (Integer i : primzahlen)
			getrennt.get(i.toString().toCharArray().length).add(i);

		int ret = 0;

		for (int i = 1; i < 7; i++) {
			for (int j : getrennt.get(i)) {
				Set<Integer> temp = circular(j);
				if (getrennt.get(i).containsAll(temp)) {
					ret += 1;
					System.out.println("found");
				}
			}

		}
		System.out.println(ret);
	}

	public static Set<Integer> circular(int zahl) {
		Set<Integer> ret = new HashSet<Integer>();
		String sZahl = Integer.toString(zahl);
		for (int i = 1; i < sZahl.length(); i++) {
			ret.add(Integer.parseInt(sZahl.substring(i) + sZahl.substring(0, i)));

		}

		return ret;
	}

	private static Set<Integer> sieb(int limit) {
		Set<Integer> primzahlen = new HashSet<>();
		boolean[] sieb = new boolean[limit + 1];
		int limitWurzel = (int) Math.sqrt((double) limit);
		Arrays.fill(sieb, false);
		sieb[0] = false;
		sieb[1] = false;
		sieb[2] = true;
		sieb[3] = true;
		for (int x = 1; x <= limitWurzel; x++) {
			for (int y = 1; y <= limitWurzel; y++) {
				int n = (4 * x * x) + (y * y);
				if (n <= limit && (n % 12 == 1 || n % 12 == 5)) {
					sieb[n] = !sieb[n];
				}

				n = (3 * x * x) + (y * y);
				if (n <= limit && (n % 12 == 7)) {
					sieb[n] = !sieb[n];
				}

				n = (3 * x * x) - (y * y);
				if (x > y && n <= limit && (n % 12 == 11)) {
					sieb[n] = !sieb[n];
				}

			}
		}
		for (int n = 5; n <= limitWurzel; n++) {
			if (sieb[n]) {
				int x = n * n;
				for (int i = x; i <= limit; i += x) {
					sieb[i] = false;
				}
			}
		}

		for (int i = 0; i <= limit; i++) {
			if (sieb[i])
				primzahlen.add(i);

		}

		return primzahlen;

	}

}
