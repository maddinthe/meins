package euler21to40;

import java.util.*;

public class Euler30DigitFithtPowers {

	public static void main(String[] args) {
		Set<Integer> nummern=new HashSet<>();
		
		for(Integer i=2;i<=9000000;i++){
			char[] z=i.toString().toCharArray();
			int probe=0;
			for(char c:z)
				probe+=Math.pow(Character.getNumericValue(c), 5);
			if (probe==i) nummern.add(i);
		}
		
		long erg=0;
		for(Integer i:nummern)
			erg+=i;

		System.out.println(erg);
		
	}

}
