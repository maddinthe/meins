package euler21to40;

import java.util.*;

public class Euler23NonAbundantSums {

	public static void main(String[] args) {
		Set<Integer> abundant = new HashSet<>();
		for (int i = 1; i < 30000; i++) {
			for (int j = 2, s = 0; j <= Math.sqrt(i); j++) {
				if (i % j == 0) {
					s += j;
					if(j*j!=i)
					s += (i / j);
				}
				if (s > i) {
					abundant.add(i);
					break;
				}
			}

		}
		Set<Integer> summen = new HashSet<>();

		for (Integer i : abundant)
			for (Integer j : abundant)
				summen.add(i + j);
		
		Set<Integer> erg = new HashSet<>();
		for (int i=1;i<28124;i++)
			erg.add(i);
		
		erg.removeAll(summen);
		
		
		long ausg=0;
		
		for(Integer i:erg)
			ausg+=i;
		
		System.out.println(ausg);

	}

}
