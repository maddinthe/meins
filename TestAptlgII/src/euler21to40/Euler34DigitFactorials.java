package euler21to40;

import java.util.*;

public class Euler34DigitFactorials {
	private static int[] fakultaeten={1,1,2,6,24,120,720,5040,40320,362880};

	public static void main(String[] args) {

		List<Integer> fatkorials=new LinkedList<>();
		
		for (int i=3;i<=2540160;i++){
			if (fak(i)){
				fatkorials.add(i);
				System.out.println("hinzu"+i);
			}
		}
		long sum=0;
		for(int i:fatkorials)
			sum+=i;
		System.out.println(sum);

		
		
		
	}

	private static boolean fak(int zahl) {

		int number = 1;
		while (zahl / Math.pow(10, number) > 1) {
			number++;
		}
		int[] ziffern = new int[number];
		for (int j = number - 1; j >= 0; j--) {
			ziffern[j] = ((int) (zahl / Math.pow(10, j) % 10));
		}
		int ret = 0;
		for (int i : ziffern) {
			ret += fakultaeten[i];
		}
		return ret == zahl;
	}

}
