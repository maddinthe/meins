package euler21to40;

public class Euler26ReziprokeZyklen {

	public static void main(String[] args) {
		int sequenzLaenge = 0;
		 
		for (int i = 1000; i > 1; i--) {
		    if (sequenzLaenge >= i) {
		        break;
		    }
		 
		    int[] gefRest = new int[i];
		    int wert = 1;
		    int pos = 0;
		 
		    while (gefRest[wert] == 0 && wert != 0) {
		        gefRest[wert] = pos;
		        wert *= 10;
		        wert %= i;
		        pos++;
		    }
		 
		    if (pos - gefRest[wert] > sequenzLaenge) {
		        sequenzLaenge = pos - gefRest[wert];
		    }
		}
		
		System.out.println(sequenzLaenge+1);
		

	}

}
