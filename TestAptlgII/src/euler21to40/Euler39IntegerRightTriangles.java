package euler21to40;

public class Euler39IntegerRightTriangles {

	public static void main(String[] args) {
		int pMax = 0, tMax = 0;
		int m = 0, k = 0;

		for (int s = 1; s <= 1000; s++) {
			int t = 0;
			int mlimit = (int) Math.sqrt(s / 2);
			for (m = 2; m <= mlimit; m++) {
				if ((s / 2) % m == 0) {
					if (m % 2 == 0) {
						k = m + 1;
					} else {
						k = m + 2;
					}
					while (k < 2 * m && k <= s / (2 * m)) {
						if (s / (2 * m) % k == 0 && ggt(k, m) == 1) {
							t++;
						}
						k += 2;
					}
				}
			}
			if (t > tMax) {
				tMax = t;
				pMax = s;
			}
		}
		System.out.println(pMax);
	}

	private static long ggt(long a, long b) {
		return (b == 0) ? a : ggt(b, a % b);
	}
}
