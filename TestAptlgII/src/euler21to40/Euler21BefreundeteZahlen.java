package euler21to40;
public class Euler21BefreundeteZahlen {

	public static void main(String[] args) {

		int sum = 0;

		for (int a = 1; a < 10000; a++) {
			int b = sumOfProperDivisors(a);
			if (b > a)
				if (sumOfProperDivisors(b) == a)
					sum += a + b;
		}

		System.out.println(sum);

	}

	public static int sumOfDivisors(int n) {
		int sum = 1;
		int p = 2;
		while (p * p <= n && n > 1) {
			if (n % p == 0) {
				int j = p * p;
				n /= p;
				while (n % p == 0) {
					j *= p;
					n /= p;
				}
				sum *= (j - 1);
				sum /= (p - 1);

			}
			if (p == 2)
				p = 3;
			else
				p += 2;
		}
		if (n > 1)
			sum *= (n + 1);
		return sum;
	}

	public static int sumOfProperDivisors(int n) {
		return sumOfDivisors(n) - n;
	}

}
