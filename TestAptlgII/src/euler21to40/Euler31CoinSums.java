package euler21to40;


public class Euler31CoinSums {

	


	public static void main(String[] args) {

		System.out.println(wege(10000, 8));

	}

	public static long wege(int geldMenge, int anzMuenzen) {
		int[] muenzen = { 1, 2, 5, 10, 20, 50, 100, 200 };
		long[] wege=new long[geldMenge+1];
		wege[0]=1;
		for(int i=0;i<muenzen.length;i++)
			for(int j=muenzen[i];j<=geldMenge;j++)
				wege[j]=wege[j]+wege[j-muenzen[i]];
		return wege[geldMenge];
		
	}

	
}
