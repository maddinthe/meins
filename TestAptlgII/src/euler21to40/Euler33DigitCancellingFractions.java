package euler21to40;

public class Euler33DigitCancellingFractions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int denproduct = 1;
		int nomproduct = 1;
		 
		for (int i = 1; i < 10; i++) {
		    for (int den = 1; den < i; den++) {
		        for (int nom = 1; nom < den; nom++) {
		            if ((nom * 10 + i) * den == nom * (i * 10 + den)) {
		                denproduct *= den;
		                nomproduct *= nom;
		            }
		        }
		    }
		}
		
		denproduct /= gcd(nomproduct, denproduct);
		
		System.out.println(denproduct);
		
		
	}

	
	
	
	
	
	public static int gcd(int a, int b) {
		int y = 0;
		int x = 0;

		if (a > b) {
			x = a;
			y = b;
		} else {
			x = b;
			y = a;
		}

		while (x % y != 0) {
			int temp = x;
			x = y;
			y = temp % x;
		}
		return y;
	}

}
