package euler21to40;
import java.io.*;
import java.util.*;

public class Euler22NameScore {

	public static void main(String[] args) {
		String line;
		List<String> namen = new LinkedList<>();
		try {
			FileReader fread = new FileReader(
					"C:\\Users\\10340827\\Desktop\\eclipse\\P022_names.txt");
			BufferedReader in = new BufferedReader(fread);
			while ((line = in.readLine()) != null)
				namen.add(line);
			in.close();
		} catch (IOException e) {
			System.out.println("IO-Fehler");
		}
		
		Collections.sort(namen);		
		int platz=1;
		long score=0;
		for (String s:namen){
			char[] name=s.toCharArray();
			int buchstSumme=0;
			for(char c:name)
				buchstSumme+=(c-64);				
			score+=(buchstSumme*platz);
			platz++;
		}
		
		
		System.out.println(score);

	}

}
