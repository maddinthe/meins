package euler21to40;

import java.util.*;

public class Euler28NumberSpiralDiagonal {

	public static void main(String[] args) {
		List<Integer> nummern =new ArrayList<>();
		nummern.add(1);
		int zaehler=1;
		for (int i=2;i<1001;){
			for (int j=0;j<4;j++){
				nummern.add(zaehler+=i);
			}i=i+2;
		}
		
		long erg=0;
		for (int i:nummern)
			erg+=i;
		
		System.out.println(erg);

	}

}
