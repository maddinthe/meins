package euler21to40;

public class Euler38PandigitalMultiples {

	public static void main(String[] args) {
		long erg = 0;
		for (long i = 9387; i >= 9234; i--) {
		    erg = verbinden(i, 2*i);
		    if(isPandigital(erg)){
		        break;
		    }
		}
		
		System.out.println(erg);

	}
	
	
	private static boolean isPandigital(long n) {
		int digits = 0;
		int count = 0;
		int tmp;

		while (n > 0) {
			tmp = digits;
			digits = digits | 1 << (int) ((n % 10) - 1);
			if (tmp == digits) {
				return false;
			}
			count++;
			n /= 10;
		}
		return digits == (1 << count) - 1;
	}

	private static long verbinden(long a, long b) {
		long c = b;
		while (c > 0) {
			a *= 10;
			c /= 10;
		}
		return a + b;
	}

}
