package euler21to40;
public class Euler24LexicoPermu {
	private static int abr = 0;
	private static long start = System.currentTimeMillis();
	public static void main(String[] args) {
		
		permutation("0123456789");
		

	}

	public static void permutation(String str) {
		permutation("", str);
	}

	private static void permutation(String prefix, String str) {
		int n = str.length();
		if (n == 0) {
			abr++;
			if (abr == 1000000){
				System.out.println(prefix);
				System.out.println(System.currentTimeMillis() - start);}
		} else {
			for (int i = 0; i < n; i++)
				permutation(prefix + str.charAt(i),
						str.substring(0, i) + str.substring(i + 1, n));
		}
	}

}
