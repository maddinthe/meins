package euler21to40;

public class Euler27QuadraticPrimes {

	public static void main(String[] args) {
		int bestNum = 0;
		int bestA = 0;
		int bestB = 0;
		for (int a = -1000; a <= 1000; a++) {
			for (int b = -1000; b <= 1000; b++) {
				int num = numberOfConsecutivePrimesGenerated(a, b);
				if (num > bestNum) {
					bestNum = num;
					bestA = a;
					bestB = b;
				}
			}
		}
		System.out.println(bestA+","+bestB+','+(bestA * bestB));
	}

	private static int numberOfConsecutivePrimesGenerated(int a, int b) {
		for (int i = 0;; i++) {
			int n = i * i + i * a + b;
			if (n < 0 || !isPrime(n))
				return i;
		}

	}

	public static boolean isPrime(int x) {
		if (x < 0)
			throw new IllegalArgumentException("Negative number");
		if (x == 0 || x == 1)
			return false;
		else if (x == 2)
			return true;
		else {
			if (x % 2 == 0)
				return false;
			for (int i = 3, end = (int) Math.sqrt(x); i <= end; i += 2) {
				if (x % i == 0)
					return false;
			}
			return true;
		}
	}
}
