package euler21to40;

import java.util.*;

public class Euler36DoubleBasePalindromes {

	public static void main(String[] args) {
		Set<Integer> decPalin=new HashSet<>();
		
		for(int i=0;i<1000000;i++){	
			if(palin(i)) decPalin.add(i);
		}

		
		Set<Integer> decBinPalin=new HashSet<>();
		
		for(int i:decPalin)
			if(palin(Integer.toBinaryString(i))) decBinPalin.add(i);
		
		int sum=0;
		for(int i:decBinPalin)
			sum+=i;
		
		System.out.println(sum);
	}
	
	
	
	
	
	
	
	private static boolean palin(int i) {
		return palin(Integer.toString(i));
	}

	private static boolean palin(String string) {
		if (string.length() < 2) {

			return true;
		}
		if (string.charAt(0) != string.charAt(string.length() - 1)) {

			return false;
		} else {
			return palin(string.substring(1, string.length() - 1));
		}
	}
	

}
