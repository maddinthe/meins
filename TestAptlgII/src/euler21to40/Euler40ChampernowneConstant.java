package euler21to40;

public class Euler40ChampernowneConstant {

	public static void main(String[] args) {
		StringBuilder sb=new StringBuilder();
		for (int i=1;sb.length()<1000001;i++){
			sb.append(i);
		}
		String s=sb.toString();
		
		int erg=1;
		erg*=Character.getNumericValue(s.charAt(0));
		erg*=Character.getNumericValue(s.charAt(9));
		erg*=Character.getNumericValue(s.charAt(99));
		erg*=Character.getNumericValue(s.charAt(999));
		erg*=Character.getNumericValue(s.charAt(9999));
		erg*=Character.getNumericValue(s.charAt(99999));
		erg*=Character.getNumericValue(s.charAt(999999));
		
		System.out.println(erg);
			

	}

}
