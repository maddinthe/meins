package euler21to40;

import java.util.*;

public class Euler37TruncatablePrimes {

	public static void main(String[] args) {
		Set<Integer> primes=sieb(740000);		
		Set<Integer> rechts=new HashSet<>();
		Set<Integer> links=new HashSet<>();
		int[] start={2,3,5,7};
		for(int i:start){
			reTrunk(i,primes,rechts);
			liTrunk(i,primes,links);
			}
		rechts.retainAll(links);
		
		int ret=0;
		for(int i:rechts)
			ret+=i;
		
		System.out.println(ret);
		
		
		
		
		
		
		
	}
	
	private static void liTrunk(int prim, Set<Integer> primes, Set<Integer> erg) {
		if (prim>740000)return;
		for (int i=1;i<10;i++){
			int test=(int) (i*(Math.pow(10, Integer.toString(prim).length()))+prim);
			if(primes.contains(test)){
				erg.add(test);
				liTrunk(test,primes,erg);
			}			
		}		
	}

	private static void reTrunk(int prim,Set<Integer> primes,Set<Integer> erg){
		if (prim>740000)return;
		for (int i=0;i<10;i++){
			int test=prim*10+i;
			if(primes.contains(test)){
				erg.add(test);
				reTrunk(test,primes,erg);
			}			
		}		
	}
		
	
	
	private static Set<Integer> sieb(int limit) {
		Set<Integer> primzahlen = new HashSet<>();
		boolean[] sieb = new boolean[limit + 1];
		int limitWurzel = (int) Math.sqrt((double) limit);
		Arrays.fill(sieb, false);
		sieb[0] = false;
		sieb[1] = false;
		sieb[2] = true;
		sieb[3] = true;
		for (int x = 1; x <= limitWurzel; x++) {
			for (int y = 1; y <= limitWurzel; y++) {
				int n = (4 * x * x) + (y * y);
				if (n <= limit && (n % 12 == 1 || n % 12 == 5)) {
					sieb[n] = !sieb[n];
				}

				n = (3 * x * x) + (y * y);
				if (n <= limit && (n % 12 == 7)) {
					sieb[n] = !sieb[n];
				}

				n = (3 * x * x) - (y * y);
				if (x > y && n <= limit && (n % 12 == 11)) {
					sieb[n] = !sieb[n];
				}

			}
		}
		for (int n = 5; n <= limitWurzel; n++) {
			if (sieb[n]) {
				int x = n * n;
				for (int i = x; i <= limit; i += x) {
					sieb[i] = false;
				}
			}
		}

		for (int i = 0; i <= limit; i++) {
			if (sieb[i])
				primzahlen.add(i);

		}

		return primzahlen;

	}


}
