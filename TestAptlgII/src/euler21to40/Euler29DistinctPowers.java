package euler21to40;

import java.math.*;
import java.util.*;

public class Euler29DistinctPowers {

	public static void main(String[] args) {
		Set<BigInteger> terms=new HashSet<>();
		int limit =100;
		for (int i=2;i<=limit;i++)
			for (int j=2;j<=limit;j++)
				terms.add((new BigInteger(Integer.toString(i))).pow(j));

		System.out.println(terms.size());
		
	}

}
