package euler1to20;
import java.util.*;

public class PrimeSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set<Integer> primzahlen = new HashSet<>();
		int limit = 2000000;
		boolean[] sieb = new boolean[limit + 1];
		int limitWurzel = (int) Math.sqrt((double) limit);
		Arrays.fill(sieb, false);
		sieb[0] = false;
		sieb[1] = false;
		sieb[2] = true;
		sieb[3] = true;
		for (int x = 1; x <= limitWurzel; x++) {
			for (int y = 1; y <= limitWurzel; y++) {
				int n = (4 * x * x) + (y * y);
				if (n <= limit && (n % 12 == 1 || n % 12 == 5)) {
					sieb[n] = !sieb[n];
				}

				n = (3 * x * x) + (y * y);
				if (n <= limit && (n % 12 == 7)) {
					sieb[n] = !sieb[n];
				}

				n = (3 * x * x) - (y * y);
				if (x > y && n <= limit && (n % 12 == 11)) {
					sieb[n] = !sieb[n];
				}

			}
		}
		for (int n = 5; n <= limitWurzel; n++) {
			if (sieb[n]) {
				int x = n * n;
				for (int i = x; i <= limit; i += x) {
					sieb[i] = false;
				}
			}
		}

		for (int i = 0; i <= limit; i++) {
			if (sieb[i])
				primzahlen.add(i);
			
			
		}
		long erg = 0;
		for (int i : primzahlen)
			erg += i;

		System.out.println(erg);

	}
}
