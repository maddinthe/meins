package euler1to20;
import java.util.*;


public class geradefibonacci {
	private static Set<Integer> zahlen=new HashSet<>();
	
	public static void main(String[] args) {
		fib();
		int ret=0;
		for(int i:zahlen)
			ret+=i;
		System.out.println(ret);
		
		
		
		
	}
	
	public static void fib(){
		zahlen.add(2);
		fib(1,2);
		
	}
	
	
	private static void fib(int first,int second){
		int neu=first+second;
		if (neu>=4000000)
			return;
		if (neu%2==0)
			zahlen.add(neu);
		fib(second,neu);
		
	}
	
	

}
