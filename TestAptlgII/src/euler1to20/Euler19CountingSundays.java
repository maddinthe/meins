package euler1to20;
public class Euler19CountingSundays {

	public static void main(String[] args) {
		int[] datum = { 1, 1, 1901 };
		int tagNr = 2;
		int ersterSonntag = 0;

		while (datum[2] < 2001) {
			if (datum[0] > 27) {
				if (datum[1] == 4 || datum[1] == 6 || datum[1] == 9
						|| datum[1] == 11) {
					if (datum[0] == 30) {
						datum[0] = 0;
						datum[1]++;
					}
				} else if (datum[1] == 2) {
					if (datum[2] % 4 != 0 && datum[2] != 1900) {
						datum[0] = 0;
						datum[1]++;
					} else {
						if (datum[0] == 29) {
							datum[0] = 0;
							datum[1]++;
						}

					}
				}

				else if (datum[1] == 12) {
					if (datum[0] == 31) {
						datum[0] = 0;
						datum[1] = 1;
						datum[2]++;
					}
				} else {
					if (datum[0] == 31) {
						datum[0] = 0;
						datum[1]++;
					}
				}

			} else if (datum[0] == 1 && tagNr % 7 == 0) {
				ersterSonntag++;
			}
			
		datum[0]++;
		tagNr++;
		}

		System.out.println(ersterSonntag);

	}

}
