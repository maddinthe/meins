package euler1to20;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Euler18MaxPathSum {

	public static void main(String[] args) {
		int[][] values = readFile("C:\\Users\\10340827\\Desktop\\eclipse\\Zahlen2.txt");
        int depth = values.length - 2;

        while ( depth >= 0) {
            for (int j = 0; j <= depth; j++) {
                values[depth][j] += Math.max( values[depth+1][j], values[depth+1][j+1]);
            }
            depth += -1;
        }
            System.out.println("The maximum path sum is: " + values[0][0]);
    }

	
//	public static int[][] readFile(){
//		int[][] ret={{75},
//				{95, 64},
//				{17, 47, 82},
//				{18, 35, 87, 10},
//				{20, 04, 82, 47, 65},
//				{19, 01, 23, 75, 03, 34},
//				{88, 02, 77, 73, 07, 63, 67},
//				{99, 65, 04, 28, 06, 16, 70, 92},
//				{41, 41, 26, 56, 83, 40, 80, 70, 33},
//				{41, 48, 72, 33, 47, 32, 37, 16, 94, 29},
//				{53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14},
//				{70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57},
//				{91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48},
//				{63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31},
//				{04, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 04, 23}};
//		
//		return ret;
//	}
	
	public static int[][] readFile(String pfad){
		String line;
		int[][] ret=null;
		
		try{
			FileReader fread=new FileReader(pfad);
			BufferedReader in =new BufferedReader(fread);
			int size=0;
			for (;(line=in.readLine())!=null;size++);
			fread=new FileReader(pfad);
			in =new BufferedReader(fread);
			ret=new int[size][];
			for (int i=0;(line=in.readLine())!=null;i++){
				String[] zahlen=line.split(" ");
				ret[i]=new int[zahlen.length];
				for (int j=0;j<zahlen.length;j++){
					ret[i][j]=Integer.parseInt(zahlen[j]);
				}
			}
				
			in.close();
		}
		catch(IOException e){
			System.out.println("IO-Fehler");
		}
		
		
		
		
		
		
		return ret;
		
	}
	
	

	}
