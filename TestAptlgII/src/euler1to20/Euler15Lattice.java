package euler1to20;

public class Euler15Lattice {

	public static void main(String[] args) {
		System.out.println(binominal(40,20));

	}
	
	
	public static long binominal (long n, long k){
		if (k > (n-k))
			k = n - k;
			long c = 1;
			for (int i = 0; i < k; i++)
			{
			c = c * (n-i);
			c = c / (i+1);	
			}
			return c;
		
	}
	

}
