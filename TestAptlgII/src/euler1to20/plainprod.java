package euler1to20;

public class plainprod {

	public static void main(String[] args) {
		int palingr = 0;
		for (int i = 999; i > 0; i--) {
			for (int j = 999; j > 0; j--) {
				int erg = i * j;
				if (palin(erg)) {
					if (erg > palingr)
						palingr = erg;
				}
			}
		}
		System.out.println(palingr);
		// System.out.println(palin(990099));

	}

	private static boolean palin(int i) {
		return palin(Integer.toString(i));
	}

	private static boolean palin(String string) {
		if (string.length() < 2) {

			return true;
		}
		if (string.charAt(0) != string.charAt(string.length() - 1)) {

			return false;
		} else {
			return palin(string.substring(1, string.length() - 1));
		}
	}

}
