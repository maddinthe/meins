package euler1to20;


public class kgv {

	public static void main(String[] args) {
	
	long ret=1;
	for(long i=2;i<21;i++){
		ret=klgv(i,ret);
		
	}
	System.out.println(ret);
	}
	
	private static long klgv(long a, long b){
		return (a*b)/ggt(Math.min(a, b),Math.max(a, b));
		
	}
	
	private static long ggt(long a,long b){
		return (b==0)?a:ggt(b,a%b);
	}

}
