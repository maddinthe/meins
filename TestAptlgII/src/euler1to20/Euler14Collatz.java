package euler1to20;

public class Euler14Collatz {

	public static void main(String[] args) {
		
		
		int max=0;
		int maxstart=0;
		for(int j=1,i=1;j<=999999;j++){
			for(long n=j;n!=1;i++){
				if (((n&1)==0)) n/=2;
				else n=3*n+1;
			}
			if (i>max){
				max=i;
				maxstart=j;
			}
			i=1;
				
		}
		
		System.out.println(max);
		System.out.println(maxstart);

	}

}
