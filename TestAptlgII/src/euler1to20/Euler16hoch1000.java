package euler1to20;
import java.math.*;


public class Euler16hoch1000 {

	public static void main(String[] args) {
		String zahl="1";
		for (int i=0;i<1000;i++)
			zahl+=0;
		BigInteger bi=new BigInteger(zahl,2);
		zahl=bi.toString();
		char[] chars=zahl.toCharArray();
		long summe=0;
		for(char c:chars)
			summe+=Character.getNumericValue(c);
		System.out.println(summe);
		
	}

}
