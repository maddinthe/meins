package euler1to20;

public class QuadratSummenDiff {

	public static void main(String[] args) {

		long summenQuadrat=0;
		long quadratSumme=0;
		
		for(int i=1;i<101;i++){
			summenQuadrat+=i*i;
			quadratSumme+=i;
		}

		quadratSumme*=quadratSumme;
	
		System.out.println(Math.max(summenQuadrat, quadratSumme)-Math.min(summenQuadrat, quadratSumme));
		

	}

}
