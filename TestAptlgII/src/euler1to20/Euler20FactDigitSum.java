package euler1to20;
import java.math.BigInteger;


public class Euler20FactDigitSum {

	public static void main(String[] args) {
		BigInteger fak=BigInteger.ONE;

		for (int i=2;i<101;i++)
			fak=fak.multiply(BigInteger.valueOf((long)i));

		char[] zahl=fak.toString().toCharArray();
		int erg=0;
		
		for (char c:zahl)
			erg+=Character.getNumericValue(c);
		
		
		System.out.println(erg);
	}
	
	

}
