package euler1to20;
import java.util.*;


public class Prim {

	public static void main(String[] args) {
		int limit=Integer.MAX_VALUE;
		int zahl;
		int zaehler;
		boolean primzahl;
		List<Integer> primzahlen=new LinkedList<>();
		
		for(zahl=2;zahl<=limit;zahl++){
			primzahl=true;
			for(zaehler=2;zaehler<=zahl/2;zaehler++){
				if (zahl%zaehler==0){
					primzahl=false;
					break;
				}
			}
		if(primzahl) primzahlen.add(zahl);
		if (primzahlen.size()>10001)
			break;
		}
		

		System.out.println(primzahlen.get(10000));
		
	}

}
