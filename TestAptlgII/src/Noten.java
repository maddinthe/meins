import java.util.*;

public class Noten {
	public static void main(String[] args) {
		List<Double> noten = new LinkedList<>();
		for (int i = 0; i < 20; i++)
			noten.add(((int) (Math.random() * 51)) / 10.0 + 1.0);
		Collections.sort(noten);
		System.out.println(noten);
		double hoechst =noten.get(0);
		while(noten.remove(hoechst));
		double niedrig =noten.get(noten.size()-1);
		while(noten.remove(niedrig));
		double gesamt=0;
		for (double d:noten)
			gesamt+=d;
		gesamt=gesamt/noten.size();
		float schnitt=Math.round(gesamt*10)/10f;
		schnitt=Math.round(schnitt*2);
		schnitt/=2;
		System.out.println(gesamt);
		System.out.println(schnitt);
		
		
		
	}

}
